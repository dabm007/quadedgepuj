#include <QuadedgeDS_default.h>
#include <QuadedgeDS_decorator.h>
#include <QuadedgeDS_items_2.h>
#include <CGAL/IO/Color.h>
#include <CGAL/N_step_adaptor.h>

struct Traits { typedef int Point_2; };
typedef CGAL_UPJ::QuadedgeDS_default<Traits,CGAL::QuadedgeDS_items_2> QDS;
typedef CGAL_UPJ::QuadedgeDS_decorator<QDS> Decorator;
typedef QDS::Quadedge_iterator                      Quadedge_iterator;
typedef CGAL::N_step_adaptor< Quadedge_iterator, 2> Iterator;

int main() {
    
    return 0;
}

