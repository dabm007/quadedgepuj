#ifndef CGAL_QUADEDGEDS_VERTEX_MAX_BASE_WITH_ID_H
#define CGAL_QUADEDGEDS_VERTEX_MAX_BASE_WITH_ID_H 1

#include <QuadedgeDS_vertex_base.h>

namespace CGAL_UPJ {

template < class Refs, class P, class ID>
class QuadedgeDS_vertex_max_base_with_id : public QuadedgeDS_vertex_base< Refs, Tag_true, P>
{
public:
    typedef QuadedgeDS_vertex_base< Refs, Tag_true, P> Base ;
    
    typedef ID size_type ;
    
    typedef P Point ;
    
private:

    size_type mID ;
    
public:

    QuadedgeDS_vertex_max_base_with_id() : mID ( size_type(-1) )  {}
    QuadedgeDS_vertex_max_base_with_id( Point const& p) : Base(p), mID ( size_type(-1) ) {}
    QuadedgeDS_vertex_max_base_with_id( Point const& p, size_type i ) : Base(p), mID(i) {}
    
    size_type&       id()       { return mID; }
    size_type const& id() const { return mID; }
};

} //namespace CGAL

#endif // CGAL_QUADEDGEDS_VERTEX_MAX_BASE_WITH_ID_H //
// EOF //
