#ifndef CGAL_QUADEDGEDS_HALFEDGE_MAX_BASE_WITH_ID_H
#define CGAL_QUADEDGEDS_HALFEDGE_MAX_BASE_WITH_ID_H 1

#include <QuadedgeDS_quadedge_base.h>

namespace CGAL_UPJ {

template < class Refs, class ID>
class QuadedgeDS_quadedge_max_base_with_id : public QuadedgeDS_quadedge_base< Refs, Tag_true, Tag_true, Tag_true >
{
public:
    typedef QuadedgeDS_quadedge_base< Refs, Tag_true, Tag_true, Tag_true> Base ;
    
    typedef typename Base::Base_base Base_base ;
    
    typedef ID size_type ;
    
private:

    size_type mID ;
    
public:

    QuadedgeDS_quadedge_max_base_with_id( size_type i = size_type(-1) ) : mID(i) {}
    
    size_type&       id()       { return mID; }
    size_type const& id() const { return mID; }
};

} //namespace CGAL

#endif // CGAL_QUADEDGEDS_QUADEDGE_MAX_BASE_WITH_ID_H //
// EOF //
