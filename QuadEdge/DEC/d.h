#ifndef D_H
#define D_H 1

#include <iostream>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <./Quadedge_triangulation_3.h>
#include <eigen3/Eigen/Eigen>

typedef CGAL::Exact_predicates_exact_constructions_kernel Kernel;
typedef CGAL_PUJ::Quadedge_triangulation_3<Kernel> QuadTriangulation;
typedef QuadTriangulation::QDS 						QDS;
typedef QuadTriangulation::Quadedge_handle        Quadedge_handle;
typedef QuadTriangulation::Quadedge_iterator Quadedge_iterator;
typedef QuadTriangulation::Face_iterator Face_iterator;
typedef QuadTriangulation::Vertex_handle                              Vertex_handle;

typedef Eigen::SparseMatrix<double> 		SparseMatrix;
typedef Eigen::SparseVector<int> 		SparseVector;
typedef Eigen::MatrixXd				MatrixXd;


namespace DEC_PUJ {
	SparseMatrix parity(QuadTriangulation& q){

		std::cout << "Faces: " << q.qds().size_of_faces() << std::endl;

		SparseMatrix trans(q.qds().size_of_faces(),1);
		SparseMatrix seq(q.qds().size_of_faces(),1);
		SparseMatrix faces(q.qds().size_of_faces(),3);

		trans.setZero();

		std::cout << "ARANGE" << std::endl;
		//ARANGE
		for(int i=0;i<q.qds().size_of_faces();i++){
			seq.insert(i,0)=i+1;
		}

		std::cout << "trans: " << trans << std::endl;
		std::cout << "seq: " << seq << std::endl;

		//DIMENSION
		Face_iterator fi = q.qds().faces_begin();
		int faceCounter = 0;
	    for ( ; fi != q.qds().faces_end(); ++ fi) {
	        std::vector<Vertex_handle> vertices = fi->get_vertices();
	        for(int it = 0; it <= vertices.size()-1; ++it){
	        	std::cout << "inserting in pos " << faceCounter << " , " << it << std::endl;
	        	faces.insert(faceCounter,it)=vertices[it]->Id()+1;
	        }
	        faceCounter++;
	    }
	    //wiseMin=faces.cwiseMin(wiseMin);
		//int pos = minCoeff
		
		std::cout << "faces: " << faces << std::endl;
		faces.makeCompressed();
		std::cout << "wise: " << faces.coeffs().minCoeff() << std::endl;

		std::cout << "Matrix: " << std::endl << MatrixXd(faces) << std::endl;

		return trans;
	}
}

#endif // D_H //
// EOF //