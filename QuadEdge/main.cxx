#include <iostream>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <CGAL/draw_triangulation_3.h>
#include <IO.h>
#include <Quadedge_triangulation_3.h>
#include <QuadedgeDS_items_3.h>
#include <QuadedgeDS_default.h>
#include <QuadedgeDS_decorator.h>
#include <DEC/d.h>

typedef CGAL::Exact_predicates_exact_constructions_kernel Kernel;
typedef Kernel::Point_3		Point_3;
typedef CGAL_PUJ::Quadedge_triangulation_3<Kernel> QuadTriangulation;
typedef QuadTriangulation::Quadedge_iterator Quadedge_iterator;
typedef QuadTriangulation::Vertex_iterator Vertex_iterator;
typedef QuadTriangulation::Face_iterator Face_iterator;
typedef QuadTriangulation::Face                                     Face;
typedef QuadTriangulation::Face_handle                              Face_handle;
typedef QuadTriangulation::Quadedge                                     Quadedge;
typedef QuadTriangulation::Quadedge_handle                              Quadedge_handle;
typedef QuadTriangulation::Quadedge_dual                                     Quadedge_dual;
typedef QuadTriangulation::Quadedge_dual_handle                              Quadedge_dual_handle;
typedef QuadTriangulation::Vertex                                     Vertex;
typedef QuadTriangulation::Vertex_handle                              Vertex_handle;
typedef CGAL_PUJ::OBJReader<QuadTriangulation>		OBJReader;


int main() {

	std::string input="../tetrahedron.obj";
	OBJReader obj;
	QuadTriangulation t;

	Point_3 p1(0,0,0);
	Point_3 p2(0,1,0);
	Point_3 p3(1,0,0);

	//Quadedge_handle q1 = t.make_triangle(p1,p2,p3);

	obj.In(input,t);

	std::cout << "quadEdge: " << t.size_of_quadedges() << std::endl;

	std::cout << "************* VERTEX ITERATOR" << std::endl;

	Vertex_iterator vi = t.qds().vertices_begin();
    for ( ; vi != t.qds().vertices_end(); ++ vi) {
        std::cout << "ID: " << vi->Id() << " Geometry: " << vi->point() << std::endl;
    }
    std::cout << "************* END" << std::endl;

	std::cout << "************* QUADEDGES ITERATOR" << std::endl;

	Quadedge_iterator i = t.qds().quadedges_begin();
    for ( ; i != t.qds().quadedges_end(); ++ i) {
        std::cout << "ID: " << i->get_ini_point()->Id() << " Geometry: " << i->get_ini_point()->point() << std::endl;
        if(i==i->get_rot()->get_rot()->get_rot()->get_rot()){
		std::cout << "E1. is valid" << std::endl;
		}else{
			std::cout << "E1. is not valid" << std::endl;
		}

		if(i==i->get_rot()->get_onext()->get_rot()->get_onext()){
			std::cout << "E2. is valid" << std::endl;
		}else{
			std::cout << "E2. is not valid" << std::endl;
		}

		if(i!=i->get_rot()->get_rot()){
			std::cout << "E3. is valid" << std::endl;
		}else{
			std::cout << "E3. is not valid" << std::endl;
		}

		//IS RING
		std::cout << "Quadedge: " << i->get_ini_point()->point() << " to " << i->get_end_point()->point() << std::endl;
		std::cout << "Onext: " << i->get_onext()->get_ini_point()->point() << " to " << i->get_onext()->get_end_point()->point() << std::endl;
		std::cout << "OnextOnext: " << i->get_onext()->get_onext()->get_ini_point()->point() << " to " << i->get_onext()->get_onext()->get_end_point()->point() << std::endl;
    }
    std::cout << "************* END" << std::endl;

    std::cout << "************* FACE ITERATOR" << std::endl;

    Face_iterator fi = t.qds().faces_begin();
    for ( ; fi != t.qds().faces_end(); ++ fi) {
        std::cout << "TRIANGLE: " << fi->triangle() << std::endl;
        std::vector<Vertex_handle> vertices = fi->get_vertices();
        for(int it = 0; it <= vertices.size()-1; ++it){
        	std::cout << "Vertex of face ID: " << vertices[it]->Id() << std::endl;
        }
    }

    std::cout << "************* END" << std::endl;

    /*
	std::cout << "Point of q1: " << q1->get_ini_point()->point() << " to " << q1->get_end_point()->point() << std::endl;
	std::cout << "Rot of q1: " << q1->get_rot()->get_primality() << std::endl;
	std::cout << "Sym of q1: " << q1->get_rot()->get_rot()->get_ini_point()->point() << " to " << q1->get_rot()->get_rot()->get_end_point()->point() << std::endl;
	std::cout << "RotInv of q1: " << q1->get_rot()->get_rot()->get_rot()->get_primality() << std::endl;

	Quadedge_handle temp = q1;

	temp = q1;

	//std::cout << "Point of q1: " << temp->get_ini_point()->point() << " to " << temp->get_end_point()->point() << std::endl;	
	//std::cout << "Point of q3: " << temp->R_next()->get_ini_point()->point() << " to " << temp->R_next()->get_end_point()->point() << std::endl;
	//std::cout << "Point of q2: " << temp->R_next()->R_next()->get_ini_point()->point() << " to " << temp->R_next()->R_next()->get_end_point()->point() << std::endl;

	std::cout << "Point of q4: " << q1->get_ini_point()->point() << " to " << q1->get_end_point()->point() << std::endl;
	std::cout << "Point of q2: " << q1->get_onext()->get_ini_point()->point() << " to " << q1->get_onext()->get_end_point()->point() << std::endl;
	std::cout << "Point of q5: " << q1->get_onext()->get_onext()->get_ini_point()->point() << " to " << q1->get_onext()->get_onext()->get_end_point()->point() << std::endl;

	//QDS qds;
	*/
	//Decorator decorator(qds);
/*
	Point_3 p1(0,0,0);
	Point_3 p2(0,1,0);
	Point_3 p3(1,0,0);

	Quadedge_handle q1 = t.make_triangle(p1,p2,p3);

	std::cout << "quadEdge: " << t.size_of_quadedges() << std::endl;
	std::cout << "dualEdges: " << t.size_of_quadedges_dual() << std::endl;
	std::cout << "vertices: " << t.size_of_vertices() << std::endl;

	std::cout << "Point of q1: " << q1->get_ini_point()->point() << " to " << q1->get_end_point()->point() << std::endl;
	std::cout << "Point of q2: " << q1->get_onext()->sym()->get_onext()->sym()->get_ini_point()->point() << " to " << q1->get_onext()->sym()->get_onext()->sym()->get_end_point()->point() << std::endl;
	std::cout << "Point of q3: " << q1->get_onext()->sym()->get_ini_point()->point() << " to " << q1->get_onext()->sym()->get_end_point()->point() << std::endl;

    if ( q1->get_onext()->get_onext() != q1){
    	std::cout << "is not a triangle" << std::endl;
    }else{
    	std::cout << "is a triangle" << std::endl;
    }

    if ( q1->get_rot()->get_onext()->get_onext() != q1->get_rot()){
    	std::cout << "Dual is not circular" << std::endl;
    }else{
    	std::cout << "Dual is circular" << std::endl;
    }

*/

	//DEC
	std::cout << "------------------------- DEC -------------------------" << std::endl;
	DEC_PUJ::parity(t);

	return 0;
}

