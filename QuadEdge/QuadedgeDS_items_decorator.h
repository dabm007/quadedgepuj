#ifndef CGAL_QUADEDGEDS_ITEMS_DECORATOR_H
#define CGAL_QUADEDGEDS_ITEMS_DECORATOR_H 1

#include <CGAL/basic.h>
#include <cstddef>

typedef CGAL::Tag_true	Tag_true;
typedef CGAL::Tag_false	Tag_false;

namespace CGAL_PUJ {

template < class p_QDS >
class QuadedgeDS_items_decorator {
public:

// TYPES
// ----------------------------------
    typedef p_QDS                                 QDS;
    typedef p_QDS                                 QuadedgeDS;
    typedef typename QDS::Traits                  Traits;
    typedef typename QDS::Vertex                  Vertex;
    typedef typename QDS::Quadedge                Quadedge;
    typedef typename QDS::Quadedge_dual                Quadedge_dual;
    typedef typename QDS::Face                    Face;

    typedef typename QDS::Vertex_handle           Vertex_handle;
    typedef typename QDS::Vertex_const_handle     Vertex_const_handle;
    typedef typename QDS::Vertex_iterator         Vertex_iterator;
    typedef typename QDS::Vertex_const_iterator   Vertex_const_iterator;

    typedef typename QDS::Quadedge_handle         Quadedge_handle;
    typedef typename QDS::Quadedge_const_handle   Quadedge_const_handle;
    typedef typename QDS::Quadedge_iterator       Quadedge_iterator;
    typedef typename QDS::Quadedge_const_iterator Quadedge_const_iterator;

    typedef typename QDS::Quadedge_dual_handle         Quadedge_dual_handle;
    typedef typename QDS::Quadedge_dual_const_handle   Quadedge_dual_const_handle;
    typedef typename QDS::Quadedge_dual_iterator       Quadedge_dual_iterator;
    typedef typename QDS::Quadedge_dual_const_iterator Quadedge_dual_const_iterator;

    typedef typename QDS::Face_handle             Face_handle;
    typedef typename QDS::Face_const_handle       Face_const_handle;
    typedef typename QDS::Face_iterator           Face_iterator;
    typedef typename QDS::Face_const_iterator     Face_const_iterator;

    typedef typename QDS::size_type               size_type;
    typedef typename QDS::difference_type         difference_type;
    typedef typename QDS::iterator_category       iterator_category;

protected:
    typedef typename Vertex::Base                 VBase;
    typedef typename Quadedge::Base               QBase;
    typedef typename Quadedge::Base_base          QBase_base;
    typedef typename Face::Base                   FBase;

public:

// CREATION
// ----------------------------------

    QuadedgeDS_items_decorator() {}

// Access Functions
// ----------------------------------

/*
    Quadedge_handle get_vertex_quadedge( Vertex_handle v) const {
        // returns the incident quadedge of v if supported,
        // `Quadedge_handle()' otherwise.
        return get_vertex_quadedge( v, Supports_vertex_quadedge());
    }


    Vertex_handle get_vertex( Quadedge_handle h) const {
        // returns the incident vertex of h if supported,
        // `Vertex_handle()' otherwise.
        return get_vertex( h, Supports_quadedge_vertex());
    }

    Quadedge_handle get_prev( Quadedge_handle h) const {
        // returns the previous quadedge of h if supported,
        // `Quadedge_handle()' otherwise.
        return get_prev( h, Supports_quadedge_prev());
    }

    Quadedge_handle find_prev( Quadedge_handle h) const {
        // returns the previous quadedge of h. Uses the `prev()' method if
        // supported or performs a search around the face using `next()'.
        return find_prev( h, Supports_quadedge_prev());
    }

    Quadedge_handle find_prev_around_vertex( Quadedge_handle h) const {
        // returns the previous quadedge of h. Uses the `prev()' method if
        // supported or performs a search around the vertex using `next()'.
        return find_prev_around_vertex( h, Supports_quadedge_prev());
    }

    Face_handle get_face( Quadedge_handle h) const {
        // returns the incident face of h if supported,
        // `Face_handle()' otherwise.
        return get_face( h, Supports_quadedge_face());
    }

    Quadedge_handle get_face_quadedge( Face_handle f) const {
        // returns the incident quadedge of f if supported,
        // `Quadedge_handle()' otherwise.
        return get_face_quadedge( f, Supports_face_quadedge());
    }
*/
// Const Access Functions
// ----------------------------------
/*
    Quadedge_const_handle
    get_vertex_quadedge( Vertex_const_handle v) const {
        return get_vertex_quadedge( v, Supports_vertex_quadedge());
    }


    Vertex_const_handle get_vertex( Quadedge_const_handle h) const {
        return get_vertex( h, Supports_quadedge_vertex());
    }

    Quadedge_const_handle get_prev( Quadedge_const_handle h) const {
        return get_prev( h, Supports_quadedge_prev());
    }

    Quadedge_const_handle find_prev( Quadedge_const_handle h) const {
        return find_prev( h, Supports_quadedge_prev());
    }

    Quadedge_const_handle
    find_prev_around_vertex( Quadedge_const_handle h) const {
        return find_prev_around_vertex( h, Supports_quadedge_prev());
    }

    Face_const_handle get_face( Quadedge_const_handle h) const {
        return get_face( h, Supports_quadedge_face());
    }

    Quadedge_const_handle get_face_quadedge( Face_const_handle f) const {
        return get_face_quadedge( f, Supports_face_quadedge());
    }
*/
// Modifying Functions (Primitives)
// ----------------------------------
/*
    void set_vertex_quadedge( Vertex_handle v, Quadedge_handle g) const {
        // sets the incident quadedge of v to g.
        set_vertex_quadedge( v, g, Supports_vertex_quadedge());
    }
*/
    void set_vertex_quadedge( Quadedge_handle h) const {
        // sets the incident quadedge of the vertex incident to h to h.
        set_vertex_quadedge( h, h);
    }

/*
    void set_vertex( Quadedge_handle h, Vertex_handle v) const {
        // sets the incident vertex of h to v.
        set_vertex(h, v, Supports_quadedge_vertex());
    }

    void set_prev( Quadedge_handle h, Quadedge_handle g) const {
        // sets the previous link of h to g.
        set_prev( h, g, Supports_quadedge_prev());
    }

    void set_face( Quadedge_handle h, Face_handle f) const {
        // sets the incident face of h to f.
        set_face(h, f, Supports_quadedge_face());
    }

    void set_face_quadedge( Face_handle f, Quadedge_handle g) const {
        // sets the incident quadedge of f to g.
        set_face_quadedge( f, g, Supports_face_quadedge());
    }
*/
    void set_face_quadedge( Quadedge_handle h) const {
        // sets the incident quadedge of the face incident to h to h.
        set_face_quadedge( h, h);
    }

// Modifying Functions (Composed)
// ----------------------------------

    void close_tip( Quadedge_handle h) const {
        // makes `h->opposite()' the successor of h.
        h->QBase::set_next( h->opposite());
        set_prev( h->opposite(), h);
    }

    void close_tip( Quadedge_handle h, Vertex_handle v) const {
        // makes `h->opposite()' the successor of h and sets the incident
        // vertex of h to v.
        h->QBase::set_next( h->opposite());
        set_prev( h->opposite(), h);
        set_vertex( h, v);
        set_vertex_quadedge( h);
    }

    void insert_tip( Quadedge_handle h, Quadedge_handle v) const {
        // inserts the tip of the edge h into the quadedges around the
        // vertex pointed to by v. Quadedge `h->opposite()' is the new
        // successor of v and `h->next()' will be set to `v->next()'. The
        // vertex of h will be set to the vertex v refers to if vertices
        // are supported.
        h->QBase::set_next( v->next());
        v->QBase::set_next( h->opposite());
        set_prev( h->next(), h);
        set_prev( h->opposite(), v);
        set_vertex( h, get_vertex( v));
    }

    void remove_tip( Quadedge_handle h) const {
        // removes the edge `h->next()->opposite()' from the quadedge
        // circle around the vertex referred to by h. The new successor
        // quadedge of h will be `h->next()->opposite()->next()'.
        h->QBase::set_next( h->next()->opposite()->next());
        set_prev( h->next(), h);
    }

    void insert_quadedge( Quadedge_handle h, Quadedge_handle f) const {
        // inserts the quadedge h between f and `f->next()'. The face of h
        // will be the one f refers to if faces are supported.
        h->QBase::set_next( f->next());
        f->QBase::set_next( h);
        set_prev( h->next(), h);
        set_prev( h, f);
        set_face( h, get_face( f));
    }

    void remove_quadedge( Quadedge_handle h) const {
        // removes edge `h->next()' from the quadedge circle around the
        // face referred to by h. The new successor of h will be
        // `h->next()->next()'.
        h->QBase::set_next( h->next()->next());
        set_prev( h->next(), h);
    }

/*
    void set_vertex_in_vertex_loop( Quadedge_handle  , Vertex_handle  ,
                                    Tag_false) const {}
void
    set_vertex_in_vertex_loop( Quadedge_handle h, Vertex_handle v) const {
        // loops around the vertex incident to h and sets all vertex
        // pointers to v. Precondition: `h != Quadedge_handle()'.
        CGAL_precondition( h != Quadedge_handle());
        set_vertex_in_vertex_loop( h, v);
    }
*/
    void set_vertex_in_vertex_loop( Quadedge_handle h, Vertex_handle v) const {
	CGAL_precondition( h != Quadedge_handle());
        CGAL_assertion_code( std::size_t termination_count = 0;)
        Quadedge_handle end = h;
        do {
            CGAL_assertion( ++termination_count != 0);
            h->QBase::set_vertex( v);
            h = h->next()->opposite();
        } while ( h != end);
    }

    
/*
    void set_face_in_face_loop( Quadedge_handle  , Face_handle  ,
                                Tag_false) const {}
void set_face_in_face_loop( Quadedge_handle h, Face_handle f) const {
        // loops around the face incident to h and sets all face pointers
        // to f. Precondition: `h != Quadedge_handle()'.
        CGAL_precondition( h != Quadedge_handle());
        set_face_in_face_loop( h, f, Supports_quadedge_face());
    }
*/
    void set_face_in_face_loop( Quadedge_handle h, Face_handle f,
                                Tag_true) const {
        CGAL_precondition( h != Quadedge_handle());
        CGAL_assertion_code( std::size_t termination_count = 0;)
        Quadedge_handle end = h;
        do {
            CGAL_assertion( ++termination_count != 0);
            h->QBase::set_face( f);
            h = h->next();
        } while ( h != end);
    }

    

    Quadedge_handle flip_edge( Quadedge_handle h) const {
        // performs an edge flip. It returns h after
        // rotating the edge h one vertex in the direction of the face
        // orientation. Precondition: `h != Quadedge_handle()' and both
        // incident faces of h are triangles.
        CGAL_precondition( h != Quadedge_handle());
        CGAL_precondition( h == h->next()->next()->next());
        CGAL_precondition( h->opposite() ==
                    h->opposite()->next()->next()->next());
        Quadedge_handle hprev = h->next()->next();
        Quadedge_handle gprev = h->opposite()->next()->next();
        remove_tip( hprev);
        remove_tip( gprev);
        set_face_quadedge(  hprev);
        set_face_quadedge(  gprev);
        set_vertex_quadedge( hprev);
        set_vertex_quadedge( gprev);
        set_face( hprev->next(), hprev->face());
        set_face( gprev->next(), gprev->face());
        hprev = hprev->next();
        gprev = gprev->next();
        insert_tip( h, gprev);
        insert_tip( h->opposite(), hprev);
        CGAL_postcondition( h == h->next()->next()->next());
        CGAL_postcondition( h->opposite() ==
                     h->opposite()->next()->next()->next());
        return h;
    }

// Implementing These Functions.
// ====================================================
// Access Functions
// ----------------------------------

    Quadedge_handle
    get_vertex_quadedge( Vertex_handle v) const {
        return v->quadedge();
    }
    Quadedge_handle
    get_vertex_quadedge( Vertex_handle v1, Vertex_handle v2) const {
        if(v1->quadedge()==Quadedge_handle()){
            return v1->quadedge();
        }else{
            Quadedge_handle tem = v1->quadedge()->get_onext();
            while(tem!=v1->quadedge()){
                if(tem->get_end_point()->point()==v2->point()){
                    return tem;
                }
                tem = tem->get_onext();
            }
        }
        return Quadedge_handle();
    }
    Vertex_handle get_vertex( Quadedge_handle h)  const {
        return h->vertex();
    }
/*
    Quadedge_handle get_prev( Quadedge_handle  , Tag_false) const {
        return Quadedge_handle();
    }
    Quadedge_handle get_prev( Quadedge_handle h, Tag_true)  const {
        return h->QBase::prev();
    }

    Quadedge_handle find_prev( Quadedge_handle h, Tag_true) const {
        return h->QBase::prev();
    }
    Quadedge_handle find_prev( Quadedge_handle h, Tag_false) const {
        Quadedge_handle g = h;
        while ( g->next() != h)
            g = g->next();
        return g;
    }

    Quadedge_handle find_prev_around_vertex( Quadedge_handle h,
                                             Tag_true) const {
        return h->QBase::prev();
    }
    Quadedge_handle find_prev_around_vertex( Quadedge_handle h,
                                             Tag_false) const {
        Quadedge_handle g = h->opposite();
        while ( g->next() != h)
            g = g->next()->opposite();
        return g;
    }
    Face_handle get_face( Quadedge_handle  , Tag_false) const {
        return Face_handle();
    }
    Quadedge_handle get_face_quadedge( Face_handle  , Tag_false) const {
        return Quadedge_handle();
    }
*/
    
    Face_handle get_face( Quadedge_handle h)  const {
        return h->face();
    }

    Quadedge_handle get_face_quadedge( Face_handle f) const {
        return f->triangle();
    }

// Const Access Functions
// ----------------------------------
/*
    Quadedge_const_handle
    get_vertex_quadedge( Vertex_const_handle  ,Tag_false) const {
        return Quadedge_const_handle();
    }
    Vertex_const_handle
    get_vertex( Quadedge_const_handle  , Tag_false) const {
        return Vertex_const_handle();
    }
*/
    Quadedge_const_handle
    get_vertex_quadedge( Vertex_const_handle v) const {
        return v->quadedge();
    }
    Quadedge_const_handle
    get_vertex_quadedge( Vertex_const_handle v1, Vertex_const_handle v2) const {
        if(v1->quadedge()==Quadedge_handle()){
            return v1->quadedge();
        }else{
            Quadedge_handle tem = v1->quadedge()->get_onext();
            while(tem!=v1->quadedge()){
                if(tem->sym()->get_ini_point()->point()==v2->point()){
                    return tem;
                }
                tem = tem->get_onext();
            }
        }
        return Quadedge_const_handle();
    }
    Vertex_const_handle
    get_vertex( Quadedge_const_handle h)  const {
        return h->vertex();
    }

/*
    Quadedge_const_handle
    get_prev( Quadedge_const_handle  , Tag_false) const {
        return Quadedge_const_handle();
    }
    Quadedge_const_handle
    get_prev( Quadedge_const_handle h, Tag_true)  const {
        return h->QBase::prev();
    }

    Quadedge_const_handle
    find_prev( Quadedge_const_handle h, Tag_true) const {
        return h->QBase::prev();
    }
    Quadedge_const_handle
    find_prev( Quadedge_const_handle h, Tag_false) const {
        Quadedge_const_handle g = h;
        while ( g->next() != h)
            g = g->next();
        return g;
    }

    Quadedge_const_handle
    find_prev_around_vertex( Quadedge_const_handle h, Tag_true) const {
        return h->QBase::prev();
    }
    Quadedge_const_handle
    find_prev_around_vertex( Quadedge_const_handle h, Tag_false) const {
        Quadedge_const_handle g = h->opposite();
        while ( g->next() != h)
            g = g->next()->opposite();
        return g;
    }
*/
    Face_const_handle
    get_face( Quadedge_const_handle  , Tag_false) const {
        return Face_const_handle();
    }
    Face_const_handle
    get_face( Quadedge_const_handle h, Tag_true)  const {
        return h->face();
    }

    Quadedge_const_handle
    get_face_quadedge( Face_const_handle  , Tag_false) const {
        return Quadedge_const_handle();
    }
    Quadedge_const_handle
    get_face_quadedge( Face_const_handle f, Tag_true) const {
        return f->quadedge();
    }

// Modifying Function Primitives
// ----------------------------------
/*
    void set_vertex_quadedge( Vertex_handle,
                              Quadedge_handle,
                              Tag_false) const {}
    void set_vertex_quadedge( Quadedge_handle,
                              Quadedge_handle,
                              Tag_false) const {}
*/
    void set_vertex_quadedge( Vertex_handle v,
                              Quadedge_handle g)  const {
        v->VBase::set_quadedge(g);
    }
    
    void set_vertex_quadedge( Quadedge_handle h,
                              Quadedge_handle g) const {
        set_vertex_quadedge( h->vertex(), g);
    }

/*
    void set_vertex( Quadedge_handle,   Vertex_handle,  Tag_false) const {}
    void set_prev( Quadedge_handle,   Quadedge_handle,  Tag_false) const {}
    void set_face( Quadedge_handle,   Face_handle,  Tag_false) const {}
    void set_prev( Quadedge_handle h, Quadedge_handle g) const {
        h->QBase::set_prev( g);
    }
    void set_face_quadedge( Face_handle,
                            Quadedge_handle,
                            Tag_false) const {}
    void set_face_quadedge( Quadedge_handle,
                            Quadedge_handle,
                            Tag_false) const {}
*/
    void set_vertex( Quadedge_handle h, Vertex_handle v) const {
        h->QBase::set_vertex(v);
    }

    void set_face( Quadedge_handle h, Face_handle f) const {
        h->QBase::set_face(f);
    }

    
    void set_face_quadedge( Face_handle f,
                            Quadedge_handle g)  const {
        f->FBase::set_quadedge(g);
    }

    void set_face_quadedge( Quadedge_handle h,
                            Quadedge_handle g) const {
        set_face_quadedge( h->triangle(), g);
    }
};

} //namespace CGAL

#endif // CGAL_QUADEDGEDS_ITEMS_DECORATOR_H //
// EOF //

