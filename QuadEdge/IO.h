#ifndef CGAL_PUJ_IO_H
#define CGAL_PUJ_IO_H 1

#include <iostream>
#include <fstream>
#include <vector>

namespace CGAL_PUJ {

//TEMPLATE CLASS FOR IN AND OUTPUT OF A TRIANGULATION
template<typename _TRefs>
class OBJReader{
public:
	//GETTING TYPES
	typedef _TRefs    			Triangulation;
	typedef typename Triangulation::Point          Point;
	typedef typename Triangulation::Quadedge_handle Quadedge_handle;
	typedef typename Triangulation::Vertex_handle Vertex_handle;
	typedef typename Triangulation::Vertex Vertex;

public:
	//FUNCTION TO READ AN INPUT.OBJ
	void In(std::string input,Triangulation& T);
	void Out(std::string input, Triangulation T);
};

}

#include "IO.hxx"

#endif // CGAL_QUADEDGEDS_QUADEDGE_BASE_H //
// EOF //
