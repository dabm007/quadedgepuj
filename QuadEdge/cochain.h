#ifndef COCHAIN_H
#define COCHAIN_H 1

#include <iostream>
#include <CGAL/Exact_predicates_exact_constructions_kernel.h>
#include <Quadedge_triangulation_3.h>
#include <list>


typedef CGAL::Exact_predicates_exact_constructions_kernel Kernel;
typedef CGAL_PUJ::Quadedge_triangulation_3<Kernel> QuadTriangulation;
typedef QuadTriangulation::Quadedge_handle        Quadedge_handle;

typedef Kernel::Point_3		Point_3;


namespace DEC_PUJ {
class cochain{
public:
	QuadTriangulation t;
	int k; //DIMENSION
	int n; //COMPLEX DIMENSION
	bool is_primal;
	
}
}

#endif // COCHAIN_H //
// EOF //