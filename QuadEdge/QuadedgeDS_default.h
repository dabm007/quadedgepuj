#ifndef CGAL_QUADEDGEDS_DEFAULT_H
#define CGAL_QUADEDGEDS_DEFAULT_H 1

#include "QuadedgeDS_items_3.h"
#include "QuadedgeDS_list.h"
#include <CGAL/memory.h>

namespace CGAL_PUJ {

template <class Traits_, class QuadedgeDSItems = QuadedgeDS_items_3, bool _TPrimality=true,
          class Alloc = CGAL_ALLOCATOR(int)>
class QuadedgeDS_default 
    : public QuadedgeDS_list< Traits_, QuadedgeDSItems,_TPrimality, Alloc> {
public:
    typedef Traits_                                          Traits;
    typedef QuadedgeDS_list<Traits_, QuadedgeDSItems,_TPrimality, Alloc> D_S;
    typedef typename D_S::size_type                           size_type;
    QuadedgeDS_default() {}
    QuadedgeDS_default( size_type v, size_type h, size_type f)
        : QuadedgeDS_list< Traits_, QuadedgeDSItems,_TPrimality, Alloc>(v,h,f) {}
};

} //namespace CGAL
#endif // CGAL_HALFEDGEDS_DEFAULT_H //
// EOF //
