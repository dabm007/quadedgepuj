#include <Quadedge_triangulation_3.h>

namespace CGAL_PUJ {

template<class _TRefs>
Quadedge_triangulation_3<_TRefs>::Quadedge_triangulation_3(){
}

//SET
template<class _TRefs>
void Quadedge_triangulation_3<_TRefs>::set_map(Quadedge_triangulation_3<_TRefs>::Quad_map map){
	quadedgeMap = map;
}

//PRINCIPAL FUNCTIONS 
template<class _TRefs>
void Quadedge_triangulation_3<_TRefs>::add_face(std::vector<Quadedge_triangulation_3<_TRefs>::Point> vertices,int v1, int v2, int v3){
	Quadedge_triangulation_3<_TRefs>::QuadEdge_handle *e1= new
		Quadedge_triangulation_3<_TRefs>::QuadEdge_handle(vertices[v1],vertices[v2]);
	Quadedge_triangulation_3<_TRefs>::QuadEdge_handle *e2= new
		Quadedge_triangulation_3<_TRefs>::QuadEdge_handle(vertices[v2],vertices[v3]);
	Quadedge_triangulation_3<_TRefs>::QuadEdge_handle *e3= new
		Quadedge_triangulation_3<_TRefs>::QuadEdge_handle(vertices[v3],vertices[v1]);

	if(quadedgeMap.count(std::make_pair(v1,v2))==0 && quadedgeMap.count(std::make_pair(v2,v1))==0){
		quadedgeMap[std::make_pair(v1,v2)]=e1;
		quadedgeMap[std::make_pair(v2,v1)]=e1->sym();
	}else{
		*e1 = quadedgeMap[std::make_pair(v1,v2)];
	}
	if(quadedgeMap.count(std::make_pair(v2,v3))==0 && quadedgeMap.count(std::make_pair(v3,v2))==0){
		quadedgeMap[std::make_pair(v2,v3)]=e2;
		quadedgeMap[std::make_pair(v3,v2)]=e2->sym();
	}else{
		*e2 = quadedgeMap[std::make_pair(v2,v3)];
	}
	if(quadedgeMap.count(std::make_pair(v3,v1))==0 && quadedgeMap.count(std::make_pair(v1,v3))==0){
		quadedgeMap[std::make_pair(v3,v1)]=e3;
		quadedgeMap[std::make_pair(v1,v3)]=e3->sym();
	}else{
		*e3 = quadedgeMap[std::make_pair(v3,v1)];
	}

	std::cout << "Face: " << (v1+1) << "," << (v2+1) << "," << (v3+1) << std::endl;
	std::cout << "e1Ini: " << e1->get_ini_point() << " e1End: " << e1->get_end_point() << std::endl;
	std::cout << "e2Ini: " << e2->get_ini_point() << " e2End: " << e2->get_end_point() << std::endl;
	std::cout << "e3Ini: " << e3->get_ini_point() << " e3End: " << e3->get_end_point() << std::endl;

	std::cout << "First splice" << std::endl; 
	e1->splice(e3->sym());
	std::cout << "Second splice" << std::endl;
	e2->splice(e1->sym());
	std::cout << "Third splice" << std::endl;
	e3->splice(e2->sym());
	std::cout << "END" << std::endl;
}
}

