#ifndef CGAL_QUADEDGEDS_VERTEX_BASE_H
#define CGAL_QUADEDGEDS_VERTEX_BASE_H 1

#include <CGAL/basic.h>

typedef CGAL::Tag_true	Tag_true;
typedef CGAL::Tag_false	Tag_false;

namespace CGAL_PUJ {

// We use Tag_false to indicate that no point type is provided.

template < class _TRefs, class _TP>
class QuadedgeDS_vertex_base{
public:
    typedef _TRefs                                 QuadedgeDS;
    typedef _TP                                 Point;
    typedef QuadedgeDS_vertex_base< _TRefs, _TP>  Base;
    typedef Tag_false                            Supports_vertex_quadedge;
    typedef Tag_false                            Supports_vertex_point;
    typedef typename _TRefs::Vertex_handle         Vertex_handle;
    typedef typename _TRefs::Vertex_const_handle   Vertex_const_handle;
    typedef typename _TRefs::Quadedge_handle       Quadedge_handle;
    typedef typename _TRefs::Quadedge_dual_handle       Quadedge_dual_handle;
    typedef typename _TRefs::Quadedge_const_handle Quadedge_const_handle;
    typedef typename _TRefs::Quadedge_dual_const_handle Quadedge_dual_const_handle;
    typedef typename _TRefs::Face_handle           Face_handle;
    typedef typename _TRefs::Face_const_handle     Face_const_handle;
    typedef typename _TRefs::Quadedge              Quadedge;
    typedef typename _TRefs::Quadedge_dual              Quadedge_dual;
    typedef typename _TRefs::Face                  Face;
private:
	Quadedge_handle qdg;
	Point p;
    unsigned int ID;
public:
	QuadedgeDS_vertex_base() {}
	QuadedgeDS_vertex_base( const Point& pp) : p(pp) {}
    QuadedgeDS_vertex_base( const Point& pp, const unsigned int& id) : p(pp),ID(id) {}
	Quadedge_handle       quadedge()                        { return qdg; }
	Quadedge_const_handle quadedge() const                  { return qdg; }
	void                  set_quadedge( Quadedge_handle q)  { qdg = q; }
	Point&                point()                           { return p; }
	const Point&          point() const                     { return p; }
    unsigned int&                Id()                           { return ID; }
    const unsigned int&          Id() const                     { return ID; }
    bool operator< (const QuadedgeDS_vertex_base& other) const {
        return ID<other.Id();
    }
};

} //namespace CGAL

#endif // CGAL_QUADEDGEDS_VERTEX_BASE_H //
// EOF //
