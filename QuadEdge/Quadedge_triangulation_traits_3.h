#ifndef CGAL_QUADEDGE_TRIANGULATION_TRAITS_3_H
#define CGAL_QUADEDGE_TRIANGULATION_TRAITS_3_H 1

#include <CGAL/license/Polyhedron.h>


#include <CGAL/basic.h>

namespace CGAL_PUJ {

template < class Kernel_ >
class Quadedge_triangulation_traits_3 {
public:
    typedef Kernel_                   Kernel;
    typedef typename Kernel::Point_3  Point_3;
    typedef typename Kernel::Triangle_3  Triangle_3;

    typedef typename Kernel::Construct_opposite_plane_3 
                                      Construct_opposite_plane_3;
private:
    Kernel m_kernel;

public:
    Quadedge_triangulation_traits_3() {}
    Quadedge_triangulation_traits_3( const Kernel& kernel) : m_kernel(kernel) {}

    Construct_opposite_plane_3 construct_opposite_plane_3_object() const {
        return m_kernel.construct_opposite_plane_3_object();
    }
};

} //namespace CGAL

#endif // CGAL_POLYHEDRON_TRAITS_3_H //
