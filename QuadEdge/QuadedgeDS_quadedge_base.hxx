#include <QuadedgeDS_quadedge_base.h>

typedef CGAL::Tag_true	Tag_true;
typedef CGAL::Tag_false	Tag_false;

namespace CGAL_PUJ{

template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::QuadedgeDS_quadedge_base_base(){}

//PRIMARY FUNCTIONS
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
void QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::set_rot(QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_dual_handle h){ 
	rot = h;
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
void QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::set_onext(QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle h){ 
	onext = h;
}

//SECUNDARY FUNCTIONS
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::sym(){ 
	return get_rot()->get_rot();
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_dual_handle QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::rot_inv(){ 
	return get_rot()->get_rot()->get_rot();
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::L_next(){ 
	return get_rot()->get_rot()->get_rot()->get_onext()->get_rot();
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::R_next(){
	return get_rot()->get_onext()->get_rot()->get_rot()->get_rot();
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::D_next(){
	return get_rot()->get_rot()->get_onext()->get_rot()->get_rot();
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::O_prev(){
	return get_rot()->get_onext()->get_rot();
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::L_prev(){
	return get_onext()->get_rot()->get_rot();
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::R_prev(){
	return get_rot()->get_rot()->get_onext();
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::D_prev(){
	return get_rot()->get_rot()->get_rot()->get_onext()->get_rot()->get_rot()->get_rot();
}

//BASIC OPERATORS
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::operator= (QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle x){
	
	set_onext(x->get_onext());
	set_rot(x->get_rot());
	set_ini_point(x->get_ini_point());
	set_end_point(x->get_end_point());

	return this;
}



//POINT REFERENCE
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
void    QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::set_ini_point(QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::Point p){ini_point=p;}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
void	QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::set_end_point(QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::Point p){end_point=p;}

//FUNCTIONS OF QUAD-EDGE
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
void QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::splice(QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle b){

	QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle swap = get_onext();
	set_onext(b->get_onext());
	b->set_onext(swap);

	QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_dual_handle swap_dual=get_onext()->get_rot()->get_onext();
	get_onext()->get_rot()->set_onext(b->get_onext()->get_rot()->get_onext());
	b->get_onext()->get_rot()->set_onext(swap_dual);

}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
void QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality,TP,TV,TF>::swap(){
	this->splice(O_prev());
	sym()->splice(sym()->O_prev());
	splice(O_prev()->L_next());
	sym()->splice(sym()->O_prev()->L_next());
}

//CLASS EXTENDED METHODS******************************************************************************************

template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::QuadedgeDS_quadedge_base(){
	set_onext(this);
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::QuadedgeDS_quadedge_base
(QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Point const& a, QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Point const& b){

	QuadedgeDS_quadedge_base<QuadedgeDS,!_TPrimality> *rotInv;
	QuadedgeDS_quadedge_base<QuadedgeDS,_TPrimality> *sym;
	QuadedgeDS_quadedge_base<QuadedgeDS,!_TPrimality> *rot;

	rotInv = new QuadedgeDS_quadedge_base<QuadedgeDS,!_TPrimality>(b,a,this,rot);
	sym = new QuadedgeDS_quadedge_base<QuadedgeDS,_TPrimality>(b,a,rotInv,this);
	rot = new QuadedgeDS_quadedge_base<QuadedgeDS,!_TPrimality>(a,b,sym,rotInv);

	set_onext(this);
	set_rot(rot);
	set_ini_point(a);
	set_end_point(b);
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::QuadedgeDS_quadedge_base
(QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_dual_handle dual, QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle sym){
	set_onext(this);
	set_rot(dual);
	dual->set_rot(sym);
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::QuadedgeDS_quadedge_base
(QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Point const& a, 
QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Point const& b, 
QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_dual_handle dual, 
QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle sym){
	set_onext(this);
	set_rot(dual);
	dual->set_rot(sym);
	set_ini_point(a);
	set_end_point(b);
}

template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
void  QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::set_onext
(QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle h)  { Base_base::set_onext(h);}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
void  QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::set_rot
(QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_dual_handle h)  { Base_base::set_rot(h);}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::sym(){ return Base_base::sym();}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_dual_handle QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::rot_inv(){ 
	return Base_base::rot_inv();
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::L_next(){ 
	return Base_base::L_next();
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::R_next(){ 
	return Base_base::R_next();
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::D_next(){ 
	return Base_base::D_next();
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::O_prev(){ 
	return Base_base::O_prev();
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::L_prev(){ 
	return Base_base::L_prev();
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::R_prev(){ 
	return Base_base::R_prev();
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::D_prev(){ 
	return Base_base::D_prev();
}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
void QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::splice
(QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle b)  { Base_base::splice(b);}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
void QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::swap()  { Base_base::swap();}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
void	QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::set_ini_point
(QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Point p){Base_base::set_ini_point(p);}
template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
void	QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::set_end_point
(QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Point p){Base_base::set_end_point(p);}

template < class _TRefs, bool _TPrimality, class TP, class TV, class TF>
typename QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::operator= 
(QuadedgeDS_quadedge_base<_TRefs,_TPrimality,TP,TV,TF>::Quadedge_handle x){
	return Base_base::operator=(x);
}

}
