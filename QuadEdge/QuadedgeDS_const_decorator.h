#ifndef CGAL_QUADEDGEDS_CONST_DECORATOR_H
#define CGAL_QUADEDGEDS_CONST_DECORATOR_H 1

#include <QuadedgeDS_items_decorator.h>
#include <vector>
#include <CGAL/IO/Verbose_ostream.h>

typedef CGAL::Verbose_ostream Verbose_ostream; 

namespace CGAL_PUJ {

template < class p_QDS >
class QuadedgeDS_const_decorator
    : public QuadedgeDS_items_decorator<p_QDS> {

// TYPES (inherited from Items_decorator, but have to be repeated)
// ---------------------------------------------------------------
public:
    typedef p_QDS                                 QDS;
    typedef p_QDS                                 QuadedgeDS;
    typedef typename QDS::Traits                  Traits;
    typedef typename QDS::Vertex                  Vertex;
    typedef typename QDS::Quadedge                Quadedge;
    typedef typename QDS::Face                    Face;

    typedef typename QDS::Vertex_handle           Vertex_handle;
    typedef typename QDS::Vertex_const_handle     Vertex_const_handle;
    typedef typename QDS::Vertex_iterator         Vertex_iterator;
    typedef typename QDS::Vertex_const_iterator   Vertex_const_iterator;

    typedef typename QDS::Quadedge_handle         Quadedge_handle;
    typedef typename QDS::Quadedge_const_handle   Quadedge_const_handle;
    typedef typename QDS::Quadedge_iterator       Quadedge_iterator;
    typedef typename QDS::Quadedge_const_iterator Quadedge_const_iterator;

    typedef typename QDS::Face_handle             Face_handle;
    typedef typename QDS::Face_const_handle       Face_const_handle;
    typedef typename QDS::Face_iterator           Face_iterator;
    typedef typename QDS::Face_const_iterator     Face_const_iterator;

    typedef typename QDS::size_type               size_type;
    typedef typename QDS::difference_type         difference_type;
    typedef typename QDS::iterator_category       iterator_category;

// The following types are equal to either `Tag_true' or `Tag_false',
// dependent whether the named feature is supported or not.

    typedef typename QDS::Supports_vertex_quadedge
                                                  Supports_vertex_quadedge;
    typedef typename QDS::Supports_quadedge_prev  Supports_quadedge_prev;
    typedef typename QDS::Supports_quadedge_vertex
                                                  Supports_quadedge_vertex;
    typedef typename QDS::Supports_quadedge_face  Supports_quadedge_face;
    typedef typename QDS::Supports_face_quadedge  Supports_face_quadedge;

    typedef typename QDS::Supports_removal        Supports_removal;


    using QuadedgeDS_items_decorator<p_QDS>::insert_tip;
    using QuadedgeDS_items_decorator<p_QDS>::get_prev;
    using QuadedgeDS_items_decorator<p_QDS>::set_prev;
    using QuadedgeDS_items_decorator<p_QDS>::get_face;
    using QuadedgeDS_items_decorator<p_QDS>::set_face;
    using QuadedgeDS_items_decorator<p_QDS>::get_vertex;
    using QuadedgeDS_items_decorator<p_QDS>::set_vertex;
    using QuadedgeDS_items_decorator<p_QDS>::get_vertex_quadedge;
    using QuadedgeDS_items_decorator<p_QDS>::set_vertex_quadedge;
    using QuadedgeDS_items_decorator<p_QDS>::get_face_quadedge;
    using QuadedgeDS_items_decorator<p_QDS>::set_face_quadedge;
    using QuadedgeDS_items_decorator<p_QDS>::set_vertex_in_vertex_loop;
    using QuadedgeDS_items_decorator<p_QDS>::set_face_in_face_loop;
    using QuadedgeDS_items_decorator<p_QDS>::insert_quadedge;

protected:
    typedef typename Vertex::Base                 VBase;
    typedef typename Quadedge::Base               QBase;
    typedef typename Quadedge::Base_base          QBase_base;
    typedef typename Face::Base                   FBase;


// PRIVATE MEMBER VARIABLES
// ----------------------------------
private:
    const p_QDS*  qds;

// CREATION
    // ----------------------------------
public:
    // No default constructor, keeps always a reference to a QDS!

    QuadedgeDS_const_decorator( const p_QDS& h) : qds(&h) {}
        // keeps internally a const reference to `qds'.

    bool is_valid( bool verb = false, int level = 0) const;
        // returns `true' if the quadedge data structure `hds' is valid
        // with respect to the `level' value as defined in the reference
        // manual. If `verbose' is `true', statistics are written to
        // `cerr'.

    bool normalized_border_is_valid( bool verb = false) const;
        // returns `true' if the border quadedges are in normalized
        // representation, which is when enumerating all quadedges with
        // the quadedge iterator the following holds: The non-border edges
        // precede the border edges. For border edges, the second quadedge
        // is a border quadedge. (The first quadedge may or may not be a
        // border quadedge.) The quadedge iterator `border_quadedges_begin
        // ()' denotes the first border edge. If `verbose' is `true',
        // statistics are written to `cerr'.
};

template < class p_QDS >
bool
QuadedgeDS_const_decorator<p_QDS>::
normalized_border_is_valid( bool verb) const {
    bool valid = true;
    Verbose_ostream verr(verb);
    verr << "begin CGAL::QuadedgeDS_const_decorator<QDS>::"
            "normalized_border_is_valid( verb=true):" << std::endl;

    Quadedge_const_iterator e = qds->quadedges_begin();
    size_type count = 0;
    while( e != qds->quadedges_end() && ! e->is_border() && !
           e->opposite()->is_border()) {
        ++e;
        ++e;
        ++count;
    }
    verr << "    non-border edges: " << count << std::endl;
    if ( e != qds->border_quadedges_begin()) {
        verr << "    first border edge does not start at "
                "border_quadedges_begin()" << std::endl;
        valid = false;
    } else {
        count = 0;
        while( valid && e != qds->quadedges_end() &&
               e->opposite()->is_border()) {
            ++e;
            ++e;
            ++count;
        }
        verr << "    border     edges: " << count << std::endl;
        verr << "    total      edges: " << qds->size_of_quadedges()/2
             << std::endl;
        if ( e != qds->quadedges_end()) {
            if ( e->is_border()) {
                verr << "    border edge " << count
                     << ": wrong orientation." << std::endl;
            }
            verr << "    the sum of full + border equals not total edges."
                 << std::endl;
            valid = false;
        }
    }
    verr << "end of CGAL::QuadedgeDS_const_decorator<QDS>::normalized_"
            "border_is_valid(): structure is "
         << ( valid ? "valid." : "NOT VALID.") << std::endl;
    return valid;
}

template < class p_QDS >
bool
QuadedgeDS_const_decorator<p_QDS>::
is_valid( bool verb, int level) const {
    Verbose_ostream verr(verb);
    verr << "begin CGAL::QuadedgeDS_const_decorator<QDS>::is_valid("
            " verb=true, level = " << level << "):" << std::endl;

    bool valid = ( 1 != (qds->size_of_quadedges() & 1));
    if ( ! valid)
        verr << "number of quadedges is odd." << std::endl;

    // All quadedges.
    Quadedge_const_iterator begin = qds->quadedges_begin();
    Quadedge_const_iterator end   = qds->quadedges_end();
    size_type  n = 0;
    size_type nb = 0;
    for( ; valid && (begin != end); begin++) {
        verr << "quadedge " << n << std::endl;
        if ( begin->is_border())
            verr << "    is border quadedge" << std::endl;
        // Pointer integrity.
        valid = valid && ( begin->next() != Quadedge_const_handle());
        valid = valid && ( begin->opposite() != Quadedge_const_handle());
        if ( ! valid) {
            verr << "    pointer integrity corrupted (ptr==0)."
                 << std::endl;
            break;
        }
        // opposite integrity.
        valid = valid && ( begin->opposite() != begin);
        valid = valid && ( begin->opposite()->opposite() == begin);
        if ( ! valid) {
            verr << "    opposite pointer integrity corrupted."
                 << std::endl;
            break;
        }
        // previous integrity.
        valid = valid && ( ! check_tag( Supports_quadedge_prev()) ||
                           get_prev(begin->next()) == begin);
        if ( ! valid) {
            verr << "    previous pointer integrity corrupted."
                 << std::endl;
            break;
        }
        if ( level > 0) {
            // vertex integrity.
            valid = valid && ( ! check_tag( Supports_quadedge_vertex())
                          || get_vertex( begin) != Vertex_const_handle());
            if ( ! valid) {
                verr << "    vertex pointer integrity corrupted."
                     << std::endl;
                break;
            }
            valid = valid && ( get_vertex( begin) ==
                               get_vertex( begin->next()->opposite()));
            if ( ! valid) {
                verr << "    vertex pointer integrity2 corrupted."
                     << std::endl;
                break;
            }
            // face integrity.
            valid = valid && ( ! check_tag( Supports_quadedge_face())
                               || begin->is_border()
                               || get_face(begin) != Face_const_handle());
            if ( ! valid) {
                verr << "    face pointer integrity corrupted."
                     << std::endl;
                break;
            }
            valid = valid && ( get_face(begin) == get_face(begin->next()));
            if ( ! valid) {
                verr << "    face pointer integrity2 corrupted."
                     << std::endl;
                break;
            }
        }
        ++n;
        if ( begin->is_border())
            ++nb;
    }
    verr << "summe border quadedges (2*nb) = " << 2 * nb << std::endl;
    if ( valid && n != qds->size_of_quadedges())
        verr << "counting quadedges failed." << std::endl;
    if ( valid && level >= 4 && (nb != qds->size_of_border_quadedges()))
        verr << "counting border quadedges failed." << std::endl;
    valid = valid && ( n  == qds->size_of_quadedges());
    valid = valid && ( level < 4 ||
                       (nb == qds->size_of_border_quadedges()));

    // All vertices.
    Vertex_const_iterator vbegin = qds->vertices_begin();
    Vertex_const_iterator vend   = qds->vertices_end();
    size_type v = 0;
    n = 0;
    for( ; valid && (vbegin != vend); ++vbegin) {
        verr << "vertex " << v << std::endl;
        // Pointer integrity.
        if ( get_vertex_quadedge( vbegin) != Quadedge_const_handle())
            valid = valid && ( ! check_tag(
                Supports_quadedge_vertex()) ||
                get_vertex( get_vertex_quadedge( vbegin)) == vbegin);
        else
            valid = valid && (! check_tag(
                                Supports_vertex_quadedge()));
        if ( ! valid) {
            verr << "    quadedge pointer in vertex corrupted."
                 << std::endl;
            break;
        }
        // cycle-around-vertex test.
        Quadedge_const_handle h = get_vertex_quadedge( vbegin);
        if ( level >= 2 && h != Quadedge_const_handle()) {
            Quadedge_const_handle g = h;
            do {
                verr << "    quadedge " << n << std::endl;
                ++n;
                h = h->next()->opposite();
                valid = valid && ( n <= qds->size_of_quadedges() && n!=0);
                if ( ! valid)
                    verr << "    too many quadedges around vertices."
                         << std::endl;
            } while ( valid && (h != g));
        }
        ++v;
    }
    if ( valid && v != qds->size_of_vertices())
        verr << "counting vertices failed." << std::endl;
    if ( valid && level >= 2 && ( check_tag( Supports_vertex_quadedge())
         && n  != qds->size_of_quadedges()))
        verr << "counting quadedges via vertices failed." << std::endl;
    valid = valid && ( v == qds->size_of_vertices());
    valid = valid && ( level < 2 ||
                       ! check_tag( Supports_vertex_quadedge()) ||
                       n  == qds->size_of_quadedges());

    // All faces.
    Face_const_iterator fbegin = qds->faces_begin();
    Face_const_iterator fend   = qds->faces_end();
    size_type f = 0;
    n = 0;
    for( ; valid && (fbegin != fend); ++fbegin) {
        verr << "face " << f << std::endl;
        // Pointer integrity.
        if ( get_face_quadedge( fbegin) != Quadedge_const_handle())
            valid = valid && ( ! check_tag(
                Supports_quadedge_face()) ||
                get_face( get_face_quadedge( fbegin)) == fbegin);
        else
            valid = valid && (! check_tag(
                Supports_face_quadedge()) || begin->is_border());
        if ( ! valid) {
            verr << "    quadedge pointer in face corrupted." << std::endl;
            break;
        }
        // cycle-around-face test.
        Quadedge_const_handle h = get_face_quadedge( fbegin);
        if ( level >= 3 && h != Quadedge_const_handle()) {
            Quadedge_const_handle g = h;
            do {
                verr << "    quadedge " << n << std::endl;
                ++n;
                h = h->next();
                valid = valid && ( n <= qds->size_of_quadedges() && n!=0);
                if ( ! valid)
                    verr << "    too many quadedges around faces."
                         << std::endl;
            } while ( valid && (h != g));
        }
        ++f;
    }
    if ( valid && f != qds->size_of_faces())
        verr << "counting faces failed." << std::endl;
    if ( valid && level >= 3 && check_tag( Supports_face_quadedge()) &&
         n + nb  != qds->size_of_quadedges())
        verr << "counting quadedges via faces failed." << std::endl;
    valid = valid && ( f == qds->size_of_faces());
    valid = valid && ( level < 3 ||
                       ! check_tag( Supports_face_quadedge()) ||
                       n + nb  == qds->size_of_quadedges());

    if ( level >= 4) {
        verr << "level 4: normalized_border_is_valid( verbose = true)"
             << std::endl;
        valid = valid && ( normalized_border_is_valid( verb));
    }
    verr << "end of CGAL::QuadedgeDS_const_decorator<QDS>::"
            "is_valid(): structure is " << ( valid ? "valid." :
            "NOT VALID.") << std::endl;
    return valid;
}

} //namespace CGAL

#endif // CGAL_QUADEDGEDS_CONST_DECORATOR_H //
// EOF //

