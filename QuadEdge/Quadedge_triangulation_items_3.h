#ifndef CGAL_QUADEDGE_TRIANGULATION_ITEMS_3_H
#define CGAL_QUADEDGE_TRIANGULATION_ITEMS_3_H 1

#include <QuadedgeDS_vertex_base.h>
#include <QuadedgeDS_quadedge_base.h>
#include <QuadedgeDS_face_base.h>

namespace CGAL_PUJ {

class Quadedge_triangulation_items_3 {
public:
    template < class _TRefs, class _TTraits>
    struct Vertex_wrapper {
        typedef typename _TTraits::Point_3 Point;
        typedef QuadedgeDS_vertex_base< _TRefs, Point> Vertex;
    };
    template < class _TRefs, class _TTraits, bool _TPrimality>
    struct Quadedge_wrapper {
        typedef QuadedgeDS_quadedge_base< _TRefs,_TPrimality>                Quadedge;
        typedef QuadedgeDS_quadedge_base< _TRefs,!_TPrimality>                Quadedge_dual;
    };
    template < class _TRefs, class _TTraits>
    struct Face_wrapper {
        typedef typename _TTraits::Triangle_3 Triangle;
        typedef QuadedgeDS_face_base< _TRefs, Triangle>   Face;
    };
};

} //namespace CGAL
#endif // CGAL_POLYHEDRON_ITEMS_3_H //
// EOF //
