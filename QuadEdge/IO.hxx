#include <IO.h>

namespace CGAL_PUJ {

//TEMPLATE CLASS FOR IN AND OUTPUT OF A TRIANGULATION
template<typename _TRefs>
//FUNCTION TO READ AN INPUT.OBJ
void OBJReader<_TRefs>::In(std::string input, OBJReader<_TRefs>::Triangulation& T){

	//VARIABLES
	std::map<std::string, std::vector<Quadedge_handle>> QuadEdge_map;
	Quadedge_handle q;

	//OPEN FILE INPUT.OBJ
	std::ifstream input_file;
	input_file.open(input);
	std::vector<Point> points;

	if(!input_file){
		std::cout << "Cannot open file: " << input << std::endl;
	}

	std::string token;
	while(!input_file.eof())
	{
		input_file >> token;

		if(token=="v")
		{
			double x, y, z;
			input_file >> x >> y >> z;
			points.push_back(Point(x,y,z));
		}else if(token == "f"){
			std::string full_line;
			std::getline(input_file, full_line);

			std::istringstream stream(full_line);
			std::vector<std::string> vertices;
			//GETTING VERTICES
			std::copy(std::istream_iterator<std::string>(stream),std::istream_iterator<std::string>(), 
			std::back_inserter(vertices));
			
			if(vertices.size()!=0){
				if(vertices.size()==3){
					//OBJ FILE IS ONE INDEX BASED
					int v1 = std::stoi(vertices[0])-1;
					int v2 = std::stoi(vertices[1])-1;
					int v3 = std::stoi(vertices[2])-1;

					std::cout << "adding face" << std::endl;

					q=T.add_face(points,v1,v2,v3);	
					
				}
			}
		}
	}

	input_file.close();
}

template<typename _TRefs>
void OBJReader<_TRefs>::Out(std::string output, OBJReader<_TRefs>::Triangulation T){

	

}
}
