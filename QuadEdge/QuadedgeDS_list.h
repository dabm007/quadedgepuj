#ifndef CGAL_QUADEDGEDS_LIST_H
#define CGAL_QUADEDGEDS_LIST_H 1

#include <CGAL/In_place_list.h>
#include <QuadedgeDS_items_decorator.h>
#include <CGAL/memory.h>
#include <CGAL/Unique_hash_map.h>
#include <cstddef>

namespace CGAL_PUJ {

template < class VertexBase>
class QuadedgeDS_in_place_list_vertex
    : public VertexBase, public CGAL::In_place_list_base<
                      QuadedgeDS_in_place_list_vertex< VertexBase> > {
public:
    typedef QuadedgeDS_in_place_list_vertex< VertexBase> Self;
    typedef typename VertexBase::Vertex_handle       Vertex_handle;
    typedef typename VertexBase::Vertex_const_handle Vertex_const_handle;
    typedef typename VertexBase::Point Point_3;
    QuadedgeDS_in_place_list_vertex() {}
    QuadedgeDS_in_place_list_vertex( const Point_3& p, const unsigned int& ID)   // down cast
        : VertexBase(p,ID) {}
    QuadedgeDS_in_place_list_vertex( const VertexBase& v)   // down cast
        : VertexBase(v) {}
    Self& operator=( const Self& v) {
        // This self written assignment avoids that assigning vertices will
        // overwrite the list linking of the target vertex.
        *((VertexBase*)this) = ((const VertexBase&)v);
        return *this;
    }
};

template < class QuadedgeBase>
class QuadedgeDS_in_place_list_quadedge
    : public QuadedgeBase, public CGAL::In_place_list_base<
                  QuadedgeDS_in_place_list_quadedge< QuadedgeBase> > {
public:
    typedef QuadedgeDS_in_place_list_quadedge< QuadedgeBase> Self;
    typedef typename QuadedgeBase::Quadedge_handle       Quadedge_handle;
    typedef typename QuadedgeBase::Quadedge_const_handle
                                                    Quadedge_const_handle;
    typedef typename QuadedgeBase::Quadedge_dual_handle       Quadedge_dual_handle;
    typedef typename QuadedgeBase::Quadedge_dual_const_handle
                                                    Quadedge_dual_const_handle;

    typedef typename QuadedgeBase::Vertex		Vertex;
    typedef typename QuadedgeBase::Vertex_handle		Vertex_handle;

    QuadedgeDS_in_place_list_quadedge() {}                   // down cast
    QuadedgeDS_in_place_list_quadedge( const QuadedgeBase& h)
        : QuadedgeBase(h) {}
    Self& operator=( const Self& h) {
        // This self written assignment avoids that assigning quadedges will
        // overwrite the list linking of the target quadedge.
        *((QuadedgeBase*)this) = ((const QuadedgeBase&)h);
        return *this;
    }
};

template < class FaceBase>
class QuadedgeDS_in_place_list_face
    : public FaceBase, public CGAL::In_place_list_base<
                            QuadedgeDS_in_place_list_face< FaceBase> > {
public:
    typedef QuadedgeDS_in_place_list_face< FaceBase>  Self;
    typedef typename FaceBase::Face_handle       Face_handle;
    typedef typename FaceBase::Face_const_handle Face_const_handle;
    QuadedgeDS_in_place_list_face() {}                   // down cast
    QuadedgeDS_in_place_list_face( const FaceBase& f) : FaceBase(f) {}
    Self& operator=( const Self& f) {
        // This self written assignment avoids that assigning faces will
        // overwrite the list linking of the target face.
        *((FaceBase*)this) = ((const FaceBase&)f);
        // this->FaceBase::operator=(f); // does not compile on SGI
        return *this;
    }
};

template < class Traits_, class QuadedgeDSItems,bool _TPrimality, class Alloc>
class QuadedgeDS_list_types {
public:
    typedef QuadedgeDS_list_types<Traits_, QuadedgeDSItems, _TPrimality, Alloc> Self;
    typedef Traits_                                    Traits;
    typedef QuadedgeDSItems                            Items;
    typedef Alloc                                      Allocator;
    typedef Alloc                                      allocator_type;

    typedef typename Items::template Vertex_wrapper<Self,Traits>
                                                       Vertex_wrapper;
    typedef typename Items::template Quadedge_wrapper<Self,Traits,_TPrimality> 
                                                       Quadedge_wrapper;
    typedef typename Items::template Face_wrapper<Self,Traits>
                                                       Face_wrapper;

    typedef typename Vertex_wrapper::Vertex            Vertex_base;
    typedef QuadedgeDS_in_place_list_vertex< Vertex_base> Vertex;
    typedef typename Quadedge_wrapper::Quadedge        Quadedge_base;
    typedef typename Quadedge_wrapper::Quadedge_dual        Quadedge_dual_base;
    typedef QuadedgeDS_in_place_list_quadedge< Quadedge_base> Quadedge;
    typedef QuadedgeDS_in_place_list_quadedge< Quadedge_dual_base> Quadedge_dual;
    typedef typename Face_wrapper::Face                Face_base;
    typedef QuadedgeDS_in_place_list_face< Face_base>  Face;

    typedef typename Allocator::template rebind< Vertex> Vertex_alloc_rebind;
    typedef typename Vertex_alloc_rebind::other        Vertex_allocator;
    typedef typename Allocator::template rebind< Quadedge>
                                                       Quadedge_alloc_rebind;
    typedef typename Allocator::template rebind< Quadedge_dual>
                                                       Quadedge_dual_alloc_rebind;
    typedef typename Quadedge_alloc_rebind::other      Quadedge_allocator;
    typedef typename Quadedge_dual_alloc_rebind::other      Quadedge_dual_allocator;
    typedef typename Allocator::template rebind< Face> Face_alloc_rebind;
    typedef typename Face_alloc_rebind::other          Face_allocator;

    typedef CGAL::In_place_list<Vertex,false,Vertex_allocator>  Vertex_list;
    typedef typename Vertex_list::iterator             Vertex_handle;
    typedef typename Vertex_list::const_iterator       Vertex_const_handle;
    typedef typename Vertex_list::iterator             Vertex_iterator;
    typedef typename Vertex_list::const_iterator       Vertex_const_iterator;

    typedef CGAL::In_place_list<Quadedge,false,Quadedge_allocator>  Quadedge_list;
    typedef typename Quadedge_list::iterator           Quadedge_handle;
    typedef typename Quadedge_list::const_iterator     Quadedge_const_handle;
    typedef typename Quadedge_list::iterator           Quadedge_iterator;
    typedef typename Quadedge_list::const_iterator     Quadedge_const_iterator;

    typedef CGAL::In_place_list<Quadedge_dual,false,Quadedge_dual_allocator>  Quadedge_dual_list;
    typedef typename Quadedge_dual_list::iterator           Quadedge_dual_handle;
    typedef typename Quadedge_dual_list::const_iterator     Quadedge_dual_const_handle;
    typedef typename Quadedge_dual_list::iterator           Quadedge_dual_iterator;
    typedef typename Quadedge_dual_list::const_iterator     Quadedge_dual_const_iterator;

    typedef CGAL::In_place_list<Face,false,Face_allocator>   Face_list;
    typedef typename Face_list::iterator               Face_handle;
    typedef typename Face_list::const_iterator         Face_const_handle;
    typedef typename Face_list::iterator               Face_iterator;
    typedef typename Face_list::const_iterator         Face_const_iterator;

    typedef typename Quadedge_list::size_type          size_type;
    typedef typename Quadedge_list::difference_type    difference_type;
    typedef std::bidirectional_iterator_tag            iterator_category;
};

template < class Traits_, class QuadedgeDSItems, bool _TPrimality = true,
           class Alloc = CGAL_ALLOCATOR(int)>
class QuadedgeDS_list
    : public QuadedgeDS_list_types<Traits_, QuadedgeDSItems, _TPrimality, Alloc> {
public:
    typedef QuadedgeDS_list<Traits_, QuadedgeDSItems,_TPrimality, Alloc> Self;
public:
    typedef QuadedgeDS_list_types<Traits_, QuadedgeDSItems,_TPrimality, Alloc> Types;
    typedef typename Types::Traits                     Traits;
    typedef typename Types::Items                      Items;
    typedef typename Types::Allocator                  Allocator;
    typedef typename Types::allocator_type             allocator_type;

    typedef typename Types::Vertex                     Vertex;
    typedef typename Types::Quadedge                   Quadedge;
    typedef typename Types::Quadedge_dual              Quadedge_dual;
    typedef typename Types::Face                       Face;

    typedef typename Types::Vertex_allocator           Vertex_allocator;
    typedef typename Types::Vertex_list                Vertex_list;
    typedef typename Types::Vertex_handle              Vertex_handle;
    typedef typename Types::Vertex_const_handle        Vertex_const_handle;
    typedef typename Types::Vertex_iterator            Vertex_iterator;
    typedef typename Types::Vertex_const_iterator      Vertex_const_iterator;

    typedef typename Types::Quadedge_allocator         Quadedge_allocator;
    typedef typename Types::Quadedge_list              Quadedge_list;
    typedef typename Types::Quadedge_handle            Quadedge_handle;
    typedef typename Types::Quadedge_const_handle      Quadedge_const_handle;
    typedef typename Types::Quadedge_iterator          Quadedge_iterator;
    typedef typename Types::Quadedge_const_iterator    Quadedge_const_iterator;

    //TYPES FOR DUAL
    typedef typename Types::Quadedge_dual_allocator         Quadedge_dual_allocator;
    typedef typename Types::Quadedge_dual_list              Quadedge_dual_list;
    typedef typename Types::Quadedge_dual_handle            Quadedge_dual_handle;
    typedef typename Types::Quadedge_dual_const_handle      Quadedge_dual_const_handle;
    typedef typename Types::Quadedge_dual_iterator          Quadedge_dual_iterator;
    typedef typename Types::Quadedge_dual_const_iterator    Quadedge_dual_const_iterator;

    typedef typename Types::Face_allocator             Face_allocator;
    typedef typename Types::Face_list                  Face_list;
    typedef typename Types::Face_handle                Face_handle;
    typedef typename Types::Face_const_handle          Face_const_handle;
    typedef typename Types::Face_iterator              Face_iterator;
    typedef typename Types::Face_const_iterator        Face_const_iterator;

    typedef typename Types::size_type                  size_type;
    typedef typename Types::difference_type            difference_type;
    typedef typename Types::iterator_category          iterator_category;

    typedef Tag_true                                   Supports_removal;

    typedef typename Allocator::template rebind<Quadedge>
                                                       Edge_alloc_rebind;
    typedef typename Edge_alloc_rebind::other          Edge_allocator;
    typedef typename Allocator::template rebind<Quadedge_dual>
                                                       Edge_dual_alloc_rebind;
    typedef typename Edge_dual_alloc_rebind::other          Edge_dual_allocator;

    typedef std::set<Vertex>            Vertex_set;
    typedef std::map<Vertex,Vertex_handle>            Vertex_map;

protected:
    // Changed from static to local variable
    Vertex_allocator vertex_allocator;
    Edge_allocator   edge_allocator;  
    Edge_dual_allocator   edge_dual_allocator;  
    Face_allocator   face_allocator;
    
    Vertex* get_vertex_node( const Vertex& t) {
        Vertex* p = vertex_allocator.allocate(1);
        vertex_allocator.construct(p, t);
        return p;
    }
    void put_vertex_node( Vertex* p) {
        vertex_allocator.destroy( p);
        vertex_allocator.deallocate( p, 1);
    }

    Quadedge* get_edge_node(const Quadedge& next, const Quadedge_dual& rot, const Quadedge& inv, const Quadedge_dual& rotInv) {
    	//PRIMALITY
            Quadedge* aNext = edge_allocator.allocate(1);
            edge_allocator.construct(aNext, next);
    	Quadedge* aInv = edge_allocator.allocate(1);
            edge_allocator.construct(aInv, inv);

    	//DUAL
    	Quadedge_dual* aRot = edge_dual_allocator.allocate(1);
            edge_dual_allocator.construct(aRot, rot);
    	Quadedge_dual* aRotInv = edge_dual_allocator.allocate(1);
            edge_dual_allocator.construct(aRotInv, rotInv);

    	aNext->QBase::set_onext(aNext);
    	aNext->QBase::set_rot(aRot);
        aInv->QBase::set_onext(aInv);
        aInv->QBase::set_rot(aRotInv);

    	aRot->QBase_dual::set_onext(aRotInv);
    	aRot->QBase_dual::set_rot(aInv);
    	aRotInv->QBase_dual::set_onext(aRot);
	    aRotInv->QBase_dual::set_rot(aNext);
        
        return aNext;
    }
    Quadedge_dual* get_edge_dual_node( const Quadedge_dual& h) {
        Quadedge_dual* ha = edge_dual_allocator.allocate(1);
        edge_dual_allocator.construct(ha, h);
        
        return ha;
    }
    void put_edge_node( Quadedge* h) {
        // deletes the pair of opposite quadedge h and h->opposite().
        //Quadedge_handle g = h->opposite();
        //Quadedge_pair* hpair = (Quadedge_pair*)(&*h);
        //if ( &*h > &*g)
        //    hpair = (Quadedge_pair*)(&*g);
        //CGAL_assertion( &(hpair->first) == (Quadedge*)hpair);
        //edge_allocator.destroy( hpair);
        //edge_allocator.deallocate( hpair, 1);
    }

    Face* get_face_node( const Face& t) {
        Face* p = face_allocator.allocate(1);
        face_allocator.construct(p, t);
        return p;
    }
    void put_face_node( Face* p) {
        face_allocator.destroy( p);
        face_allocator.deallocate( p, 1);
    }

    typedef typename Vertex::Base                      VBase;
    typedef typename Quadedge::Base                    QBase;
    typedef typename Quadedge::Base_base               QBase_base;
    typedef typename Quadedge_dual::Base                    QBase_dual;
    typedef typename Quadedge_dual::Base_base               QBase_dual_base;
    typedef typename Face::Base                        FBase;

    Vertex_list        vertices;
    Quadedge_list      quadedges;
    Quadedge_dual_list      quadedges_dual;
    Face_list          faces;
    Vertex_set         vertices_set;
    Vertex_map         vertices_map;


    size_type          nb_border_quadedges;
    size_type          nb_border_edges;
    Quadedge_iterator  border_quadedges;

// CREATION

private:
    void pointer_update( const Self& qds) {
        // Update own pointers assuming that they lived previously
        // in a Quadedge data structure `qds' with lists.

        typedef CGAL::Unique_hash_map< Vertex_const_iterator, Vertex_iterator> V_map;
        typedef CGAL::Unique_hash_map< Quadedge_const_iterator, Quadedge_iterator>
                                                                         H_map;
        typedef CGAL::Unique_hash_map< Face_const_iterator, Face_iterator>     F_map;
        // initialize maps.
        H_map h_map( qds.quadedges_begin(), qds.quadedges_end(),
                     quadedges_begin(), Quadedge_iterator(), 
                     3 * qds.size_of_quadedges() / 2);
        Vertex_iterator vii;
        V_map v_map( vii, 3 * qds.size_of_vertices() / 2);
        Face_iterator fii;
        F_map f_map( fii, 3 * qds.size_of_faces() / 2);
        // some special values
        h_map[Quadedge_const_iterator()] = Quadedge_iterator();
        h_map[qds.quadedges_end()]       = quadedges_end();
        v_map[Vertex_const_iterator()]   = Vertex_iterator();
        v_map[qds.vertices_end()]        = vertices_end();
        f_map[Face_const_iterator()]     = Face_iterator();
        f_map[qds.faces_end()]           = faces_end();
        // vertices and faces are optional
        if (true) {
            v_map.insert( qds.vertices_begin(),
                          qds.vertices_end(),
                          vertices_begin());
        }
        if ( true) {
            f_map.insert( qds.faces_begin(), qds.faces_end(), faces_begin());
        }
        QuadedgeDS_items_decorator<Self> D;
        for (Quadedge_iterator h = quadedges_begin(); h!=quadedges_end(); ++h){
            h->QBase::set_next( h_map[ h->next()]);
            // Superfluous and false: opposite pointer get set upon creation
            // h->QBase_base::set_opposite( h_map[ h->opposite()]);
            if (true)
                D.set_prev( h, h_map[ D.get_prev(h)]);
            if (true)
                D.set_vertex( h, v_map[ D.get_vertex(h)]);
            if (true)
                D.set_face( h, f_map[ D.get_face(h)]);
        }
        border_quadedges = h_map[ border_quadedges];
        if (true) {
            for (Vertex_iterator v = vertices_begin(); v != vertices_end();++v)
                D.set_vertex_quadedge(v, h_map[ D.get_vertex_quadedge(v)]);
        }
        if (true) {
            for ( Face_iterator f = faces_begin(); f != faces_end(); ++f)
                D.set_face_quadedge(f, h_map[ D.get_face_quadedge(f)]);
        }
        //h_map.statistics();
        //v_map.statistics();
        //f_map.statistics();
    }

public:
    QuadedgeDS_list()
        : nb_border_quadedges(0), nb_border_edges(0) {}
        // the empty polyhedron `P'.

    QuadedgeDS_list( size_type, size_type, size_type)
        : nb_border_quadedges(0), nb_border_edges(0) {}
        // Parameter order is v,h,f.
        // a polyhedron `P' with storage reserved for v vertices, h
        // quadedges, and f faces. The reservation sizes are a hint for
        // optimizing storage allocation. They are not used here.

    ~QuadedgeDS_list() { 
	//clear(); 
     }

    QuadedgeDS_list( const Self& qds)
    :  vertices( qds.vertices),
       //quadedge( qds.quadedges),
       faces( qds.faces),
       nb_border_quadedges( qds.nb_border_quadedges),
       nb_border_edges( qds.nb_border_edges),
       border_quadedges( qds.border_quadedges)
    {
        // goal is quadedges = qds.quadedges, but we have pairs here
        Quadedge_const_iterator i = qds.quadedges_begin();
        for ( ; i != qds.quadedges_end(); ++ ++ i) {
            edges_push_back(*i,*i->get_rot(),*i->get_rot()->get_rot(),*i->get_rot()->get_rot()->get_rot());
        }
        //pointer_update( qds);
    }

    Self& operator=( const Self& qds)  {
        if ( this != &qds) {
            //clear();
            vertices            = qds.vertices;
            // goal is quadedges = qds.quadedges, but we have pairs here
            quadedges = Quadedge_list();
            Quadedge_const_iterator i = qds.quadedges_begin();
            for ( ; i != qds.quadedges_end(); ++ ++ i) {
                edges_push_back(*i,*i->get_rot(),*i->get_rot()->get_rot(),*i->get_rot()->get_rot()->get_rot());
            }
            faces               = qds.faces;
            nb_border_quadedges = qds.nb_border_quadedges;
            nb_border_edges     = qds.nb_border_edges;
            border_quadedges    = qds.border_quadedges;
            //pointer_update( qds);
        }
        return *this;
    }

    void reserve( size_type, size_type, size_type) {}
        // Parameter order is v,h,f.
        // reserve storage for v vertices, h quadedges, and f faces. The
        // reservation sizes are a hint for optimizing storage allocation.
        // If the `capacity' is already greater than the requested size
        // nothing happens. If the `capacity' changes all iterators and
        // circulators invalidates. The function is void here.

// Access Member Functions

    allocator_type  get_allocator() const { return allocator_type(); }

    size_type size_of_vertices() const  { return vertices.size();}
    size_type size_of_quadedges() const { return quadedges.size();}
    size_type size_of_quadedges_dual() const { return quadedges_dual.size();}

    size_type size_of_faces() const     { return faces.size();}

    size_type capacity_of_vertices() const    { return vertices.max_size();}
    size_type capacity_of_quadedges() const   { return quadedges.max_size();}
    size_type capacity_of_faces() const       { return faces.max_size();}

    std::size_t bytes() const {
        return sizeof(Self)
               + vertices.size()  * sizeof( Vertex)
               + quadedges.size() * sizeof( Quadedge)
               + faces.size()     * sizeof( Face);
    }
    std::size_t bytes_reserved() const { return bytes();}

    Vertex_iterator   vertices_begin()   { return vertices.begin();}
    Vertex_iterator   vertices_end()     { return vertices.end();}
    Quadedge_iterator quadedges_begin()  { return quadedges.begin();}
    Quadedge_iterator quadedges_end()    { return quadedges.end();}
    Face_iterator     faces_begin()      { return faces.begin();}
    Face_iterator     faces_end()        { return faces.end();}

    // The constant iterators and circulators.

    Vertex_const_iterator   vertices_begin()  const{ return vertices.begin();}
    Vertex_const_iterator   vertices_end()    const{ return vertices.end();}
    Quadedge_const_iterator quadedges_begin() const{ return quadedges.begin();}
    Quadedge_const_iterator quadedges_end()   const{ return quadedges.end();}
    Face_const_iterator     faces_begin()     const{ return faces.begin();}
    Face_const_iterator     faces_end()       const{ return faces.end();}

// Insertion
//
// The following operations simply allocate a new element of that type.
// Quadedge are always allocated in pairs of opposite quadedges. The
// opposite pointers are automatically set.

    Vertex_handle vertices_push_back( const Vertex& v) {

        if(vertices_set.insert(v).second){
            vertices.push_back(* get_vertex_node(*vertices_set.insert(v).first));
            Vertex_handle vh = vertices.end();
            --vh;
            vertices_map.insert(std::pair<Vertex,Vertex_handle>(v,vh));
        }
        
        return vertices_map.find(v)->second;
    }

/*
    Quadedge_handle edges_push_back( const Quadedge& h, const Quadedge& g) {
        // creates a new pair of opposite border quadedge.
        Quadedge* ptr = get_edge_node( h, g);
        quadedges.push_back( *ptr);
        Quadedge_handle hh = quadedges.end();
        --hh;
        quadedges.push_back( *(ptr->opposite()));
        return hh;
    }
*/

    Quadedge_handle edges_push_back( const Quadedge& next, const Quadedge_dual& rot, const Quadedge& inv, const Quadedge_dual& rotInv) {
    	
        Quadedge* ptr = get_edge_node(next,rot,inv,rotInv);
        
        std::cout<< "add edge" << std::endl;
        quadedges.push_back(*ptr);

    	Quadedge_handle hh = quadedges.end();

    	quadedges_dual.push_back(*(ptr->get_rot()));

        //quadedges.push_back(*(ptr->get_rot()->get_rot()->get_onext()));

    	//quadedges_dual.push_back( *(ptr->get_rot()->get_rot()->get_rot()));

    	return --hh;
    }

    Face_handle faces_push_back( const Face& f) {
        faces.push_back( * get_face_node(f));
        Face_handle fh = faces.end();
        return --fh;
    }


// Removal
//
// The following operations erase an element referenced by a handle.
// Quadedges are always deallocated in pairs of opposite quadedge. Erase
// of single elements is optional. The deletion of all is mandatory.

    void vertices_pop_front() {
        Vertex* v = &(vertices.front());
        vertices.pop_front();
        put_vertex_node( v);
    }
    void vertices_pop_back() {
        Vertex* v = &(vertices.back());
        vertices.pop_back();
        put_vertex_node( v);
    }
    void vertices_erase( Vertex_handle v) {
        Vertex* ptr = &*v;
        vertices.erase(v);
        put_vertex_node( ptr);
    }
    void vertices_erase( Vertex_iterator first, Vertex_iterator last) {
        while (first != last)
            vertices_erase(first++);
    }

    void edges_erase( Quadedge_handle h) {
        // deletes the pair of opposite quadedges h and h->opposite().
        //Quadedge_handle g = h->sym();
        quadedges.erase(h);
        //quadedges.erase(g);
        put_edge_node(&*h);
    }
    void edges_pop_front() { edges_erase( quadedges.begin()); }
    void edges_pop_back()  {
        Quadedge_iterator h = quadedges.end();
        edges_erase( --h);
    }
    void edges_erase( Quadedge_iterator first, Quadedge_iterator last) {
        while (first != last) {
            Quadedge_iterator nxt = first;
            ++nxt;
            CGAL_assertion( nxt != last);
            ++nxt;
            edges_erase(first);
            first = nxt;
        }
    }

    void faces_pop_front() {
        Face* f = &(faces.front());
        faces.pop_front();
        put_face_node( f);
    }
    void faces_pop_back() {
        Face* f = &(faces.back());
        faces.pop_back();
        put_face_node( f);
    }
    void faces_erase( Face_handle f) {
        Face* ptr = &*f;
        faces.erase(f);
        put_face_node( ptr);
    }
    void faces_erase( Face_iterator first, Face_iterator last) {
        while (first != last)
            faces_erase(first++);
    }

    void vertices_clear() { vertices.destroy(); }
    void edges_clear() {
        edges_erase( quadedges.begin(), quadedges.end());
        nb_border_quadedges = 0;
        nb_border_edges = 0;
        border_quadedges = Quadedge_handle();
    }
    void faces_clear() { faces.destroy(); }
    void clear() {
        vertices_clear();
        edges_clear();
        faces_clear();
    }

    void vertices_splice( Vertex_iterator target, Self &source,
                          Vertex_iterator begin, Vertex_iterator end) {
        vertices.splice( target, source.vertices, begin, end);
    }

    void quadedges_splice( Quadedge_iterator target, Self &source,
                           Quadedge_iterator begin, Quadedge_iterator end) {
        quadedges.splice( target, source.quadedges, begin, end);
    }

    void faces_splice( Face_iterator target, Self &source,
                       Face_iterator begin, Face_iterator end) {
        faces.splice( target, source.faces, begin, end);
    }

// Operations with Border Quadedge

    size_type size_of_border_quadedges() const { return nb_border_quadedges;}
        // number of border quadedges. An edge with no incident face
        // counts as two border quadedges. Precondition: `normalize_border()'
        // has been called and no quadedge insertion or removal and no
        // change in border status of the quadedges have occured since
        // then.

    size_type size_of_border_edges() const { return nb_border_edges;}
        // number of border edges. If `size_of_border_edges() ==
        // size_of_border_quadedges()' all border edges are incident to a
        // face on one side and to a hole on the other side.
        // Precondition: `normalize_border()' has been called and no
        // quadedge insertion or removal and no change in border status of
        // the quadedges have occured since then.

    Quadedge_iterator border_quadedges_begin() {
        // quadedge iterator starting with the border edges. The range [
        // `quadedges_begin(), border_quadedges_begin()') denotes all
        // non-border edges. The range [`border_quadedges_begin(),
        // quadedges_end()') denotes all border edges. Precondition:
        // `normalize_border()' has been called and no quadedge insertion
        // or removal and no change in border status of the quadedges have
        // occured since then.
        return border_quadedges;
    }

    Quadedge_const_iterator border_quadedges_begin() const {
        return border_quadedges;
    }

    void normalize_border() {
        // sorts quadedges such that the non-border edges precedes the
        // border edges. For each border edge that is incident to a face
        // the quadedge iterator will reference the quadedge incident to
        // the face right before the quadedge incident to the hole.
        CGAL_assertion_code( size_type count = quadedges.size();)
        nb_border_quadedges = 0;
        nb_border_edges = 0;
        Quadedge_list  border;
        Quadedge_iterator i = quadedges_begin();
        while ( i != quadedges_end()) {
            Quadedge_iterator j = i;
            ++i;
            ++i;
            Quadedge_iterator k = j;
            ++k;
            if ( j->is_border()) {
                nb_border_quadedges++;
                nb_border_edges++;
                if (k->is_border())
                    nb_border_quadedges++;
                border.splice( border.end(), quadedges, k);
                border.splice( border.end(), quadedges, j);
            } else if ( k->is_border()) {
                nb_border_quadedges++;
                nb_border_edges++;
                border.splice( border.end(), quadedges, j);
                border.splice( border.end(), quadedges, k);
            } else {
                CGAL_assertion_code( count -= 2;)
            }
        }
        CGAL_assertion( count == 2 * nb_border_edges );
        CGAL_assertion( count == border.size());
        if ( i == quadedges_begin()) {
            quadedges.splice( quadedges.end(), border);
            i = quadedges_begin();
        } else {
            --i;
            --i;
            CGAL_assertion( ! i->is_border() && ! i->opposite()->is_border());
            quadedges.splice( quadedges.end(), border);
            ++i;
            ++i;
        }
        CGAL_assertion( i == quadedges_end() || i->opposite()->is_border());
        border_quadedges = i;
    }
};


//  #ifndef CGAL_CFG_NO_TMPL_IN_TMPL_PARAM
//  #define CGAL__HDS_IP_List QuadedgeDS_list
//  #else
//  #define CGAL__HDS_IP_List QuadedgeDS_list::HDS
//  #endif

// init static member allocator objects (no longer static)
//template < class Traits_, class QuadedgeDSItems, class Alloc>
//typename CGAL__HDS_IP_List<Traits_, QuadedgeDSItems, Alloc>::Vertex_allocator
//CGAL__HDS_IP_List<Traits_, QuadedgeDSItems, Alloc>::vertex_allocator;
//
//template < class Traits_, class QuadedgeDSItems, class Alloc>
//typename CGAL__HDS_IP_List<Traits_, QuadedgeDSItems, Alloc>::Edge_allocator
//CGAL__HDS_IP_List<Traits_, QuadedgeDSItems, Alloc>::edge_allocator;
//
//template < class Traits_, class QuadedgeDSItems, class Alloc>
//typename CGAL__HDS_IP_List<Traits_, QuadedgeDSItems, Alloc>::Face_allocator
//CGAL__HDS_IP_List<Traits_, QuadedgeDSItems, Alloc>::face_allocator;


//  #undef CGAL__HDS_IP_List

} //namespace CGAL
#endif // CGAL_QUADEDGEDS_LIST_H //
// EOF //
