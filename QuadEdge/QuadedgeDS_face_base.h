#ifndef CGAL_QUADEDGEDS_FACE_BASE_H
#define CGAL_QUADEDGEDS_FACE_BASE_H 1

#include <CGAL/basic.h>

namespace CGAL_PUJ {

// We use Tag_false to indicate that no plane type is provided.

template < class _TRefs, class _TTriangle>
class QuadedgeDS_face_base {
public:
    typedef _TRefs                                 QuadedgeDS;
    typedef QuadedgeDS_face_base< _TRefs, Tag_false>  Base;
    typedef _TTriangle				 Triangle;
    typedef typename _TRefs::Vertex_handle         Vertex_handle;
    typedef typename _TRefs::Vertex_const_handle   Vertex_const_handle;
    typedef typename _TRefs::Quadedge_handle       Quadedge_handle;
    typedef typename _TRefs::Quadedge_const_handle Quadedge_const_handle;
    typedef typename _TRefs::Face_handle           Face_handle;
    typedef typename _TRefs::Face_const_handle     Face_const_handle;
    typedef typename _TRefs::Vertex                Vertex;
    typedef typename _TRefs::Traits::Point_3                 Point;
    typedef typename _TRefs::Quadedge              Quadedge;
private:
    Quadedge_handle qdg;
    Triangle           t;
    std::vector<Vertex_handle> vertices;
public:
    QuadedgeDS_face_base() {}
    QuadedgeDS_face_base( const Triangle& g) : t(g) {}
    Quadedge_handle       quadedge()                        { return qdg; }
    Quadedge_const_handle quadedge() const                  { return qdg; }
    void                  set_quadedge( Quadedge_handle h)  { qdg = h; }
    void                  set_triangle(Point& p1, Point& p2, Point& p3) 
    { 
        t = Triangle(p1,p2,p3);
        vertices.push_back(Vertex_handle(p1,0));
        vertices.push_back(Vertex_handle(p2,1));
        vertices.push_back(Vertex_handle(p3,2));
    }
    void                  set_triangle(Vertex_handle p1, Vertex_handle p2, Vertex_handle p3) 
    { 
        t = Triangle(p1->point(),p2->point(),p3->point());
        vertices.push_back(p1);
        vertices.push_back(p2);
        vertices.push_back(p3);
    }
    Triangle&                triangle()                           { return t; }
    const Triangle&          triangle() const                     { return t; }
    std::vector<Vertex_handle>&                get_vertices()                           { return vertices; }
    const std::vector<Vertex_handle>&          get_vertices() const                     { return vertices; }
    
};

} //namespace CGAL

#endif // CGAL_QUADEDGEDS_FACE_BASE_H //
// EOF //
