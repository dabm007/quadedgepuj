#ifndef CGAL_QUADEDGEDS_QUADEDGE_BASE_H
#define CGAL_QUADEDGEDS_QUADEDGE_BASE_H 1

typedef CGAL::Tag_true	Tag_true;
typedef CGAL::Tag_false	Tag_false;

namespace CGAL_PUJ {

template < class _TRefs, bool _TPrimality>
struct QuadedgeDS_quadedge_base_base {
	typedef _TRefs                               QuadedgeDS;
	typedef typename _TRefs::Vertex_handle			 Vertex_handle;
        typedef typename _TRefs::Vertex_const_handle   Vertex_const_handle;
	typedef typename _TRefs::Quadedge_handle	 Quadedge_handle;
	typedef typename _TRefs::Quadedge_dual_handle Quadedge_dual_handle;
	typedef typename _TRefs::Quadedge_const_handle Quadedge_const_handle;
	typedef typename _TRefs::Quadedge_dual_const_handle Quadedge_dual_const_handle;
	typedef typename _TRefs::Face_handle			 Face_handle;
        typedef typename _TRefs::Face_const_handle     Face_const_handle;
        typedef typename _TRefs::Vertex                Vertex;
        typedef typename _TRefs::Face                  Face;
private:
	Vertex_handle	vertex_ini;
	Face_handle left_face;
public:
	QuadedgeDS_quadedge_base_base(){}
	virtual ~QuadedgeDS_quadedge_base_base() {}

	//PRIMALITY
	bool get_primality() {return _TPrimality;}
	//SET
	void set_vertex(Vertex_handle v){vertex_ini=v;}
	
	//POINT REFERENCE
	//GET
	Vertex_handle	get_ini_point(){return vertex_ini;}
	//SET
	void    set_ini_point(Vertex_handle p){vertex_ini=p;}
	
};

template < class _TRefs, bool _TPrimality>
class QuadedgeDS_quadedge_base;

template < class _TRefs>
class QuadedgeDS_quadedge_base<_TRefs,true>
//PRIMALITY 
//true => Primal
//false => Dual
    : public QuadedgeDS_quadedge_base_base<_TRefs, true>
{
public:
	typedef _TRefs                               QuadedgeDS;
	typedef typename _TRefs::Vertex_handle			 Vertex_handle;
        typedef typename _TRefs::Vertex_const_handle   Vertex_const_handle;
	typedef typename _TRefs::Quadedge_handle	 Quadedge_handle;
	typedef typename _TRefs::Quadedge_dual_handle Quadedge_dual_handle;
	typedef typename _TRefs::Quadedge_const_handle Quadedge_const_handle;
	typedef typename _TRefs::Quadedge_dual_const_handle Quadedge_dual_const_handle;
	typedef typename _TRefs::Face_handle			 Face_handle;
        typedef typename _TRefs::Face_const_handle     Face_const_handle;
        typedef typename _TRefs::Vertex                Vertex;
        typedef typename _TRefs::Face                  Face;
	typedef QuadedgeDS_quadedge_base_base<_TRefs, true>    Base_base;
	typedef QuadedgeDS_quadedge_base<_TRefs,true> Base;
	typedef QuadedgeDS_quadedge_base_base<_TRefs, false>    Base_dual_base;
	typedef QuadedgeDS_quadedge_base<_TRefs,false> Base_dual;
private:
	Quadedge_handle  onext;
	Quadedge_dual_handle  rot;
public:
	//CONSTRUCTORS
	QuadedgeDS_quadedge_base(){}

	//PRIMARY FUNCTIONS
	Quadedge_dual_handle get_rot(){ return rot;}
	Quadedge_dual_const_handle get_rot() const { return rot;}
	Quadedge_handle get_onext(){ return onext;}
	Quadedge_const_handle get_onext() const { return onext;}

	//SECUNDARY FUNCTIONS
	Quadedge_handle sym(){return get_rot()->get_rot();}
	Quadedge_dual_handle rot_inv(){return get_rot()->get_rot()->get_rot();}
	Quadedge_handle L_next(){return get_rot()->get_rot()->get_rot()->get_onext()->get_rot();}
	Quadedge_handle R_next(){return get_rot()->get_onext()->get_rot()->get_rot()->get_rot();}
	Quadedge_handle D_next(){return get_rot()->get_rot()->get_onext()->get_rot()->get_rot();}
	Quadedge_handle O_prev(){return get_rot()->get_onext()->get_rot();}
	Quadedge_handle L_prev(){return get_onext()->get_rot()->get_rot();}
	Quadedge_handle R_prev(){return get_rot()->get_rot()->get_onext();}
	Quadedge_handle D_prev(){return get_rot()->get_rot()->get_rot()->get_onext()->get_rot()->get_rot()->get_rot();}
	Vertex_handle	get_end_point(){return sym()->get_ini_point();}

	//SET
	void set_rot (Quadedge_dual_handle h) {rot = h;}
	void set_onext (Quadedge_handle h) {onext = h;}
};
template < class _TRefs>
class QuadedgeDS_quadedge_base<_TRefs,false>
//PRIMALITY 
//true => Primal
//false => Dual
    : public QuadedgeDS_quadedge_base_base<_TRefs, false>
{
public:
	typedef _TRefs                               QuadedgeDS;
	typedef typename _TRefs::Vertex_handle			 Vertex_handle;
        typedef typename _TRefs::Vertex_const_handle   Vertex_const_handle;
	typedef typename _TRefs::Quadedge_handle	 Quadedge_handle;
	typedef typename _TRefs::Quadedge_dual_handle Quadedge_dual_handle;
	typedef typename _TRefs::Quadedge_const_handle Quadedge_const_handle;
	typedef typename _TRefs::Quadedge_dual_const_handle Quadedge_dual_const_handle;
	typedef typename _TRefs::Face_handle			 Face_handle;
        typedef typename _TRefs::Face_const_handle     Face_const_handle;
        typedef typename _TRefs::Vertex                Vertex;
        typedef typename _TRefs::Face                  Face;
	typedef QuadedgeDS_quadedge_base_base<_TRefs, false>    Base_base;
	typedef QuadedgeDS_quadedge_base<_TRefs,false> Base;
private:
	Quadedge_dual_handle  onext;
	Quadedge_handle  rot;
public:
	//CONSTRUCTORS
	QuadedgeDS_quadedge_base(){}

	//PRIMARY FUNCTIONS
	Quadedge_handle get_rot(){ return rot;}
	Quadedge_const_handle get_rot() const { return rot;}
	Quadedge_dual_handle get_onext(){ return onext;}
	Quadedge_dual_const_handle get_onext() const { return onext;}
	
	//SECUNDARY FUNCTIONS
	Quadedge_dual_handle sym(){return get_rot()->get_rot();}
	Quadedge_handle rot_inv(){return get_rot()->get_rot()->get_rot();}
	Quadedge_dual_handle L_next(){return get_rot()->get_rot()->get_rot()->get_onext()->get_rot();}
	Quadedge_dual_handle R_next(){return get_rot()->get_onext()->get_rot()->get_rot()->get_rot();}
	Quadedge_dual_handle D_next(){return get_rot()->get_rot()->get_onext()->get_rot()->get_rot();}
	Quadedge_dual_handle O_prev(){return get_rot()->get_onext()->get_rot();}
	Quadedge_dual_handle L_prev(){return get_onext()->get_rot()->get_rot();}
	Quadedge_dual_handle R_prev(){return get_rot()->get_rot()->get_onext();}
	Quadedge_dual_handle D_prev(){return get_rot()->get_rot()->get_rot()->get_onext()->get_rot()->get_rot()->get_rot();}
	Vertex_handle	get_end_point(){return sym()->get_ini_point();}

	//SET
	void set_rot (Quadedge_handle h) {rot = h;}
	void set_onext (Quadedge_dual_handle h) {onext = h;}
};

} //namespace CGAL_PUJ

//#include "QuadedgeDS_quadedge_base.hxx"

#endif // CGAL_QUADEDGEDS_QUADEDGE_BASE_H //
// EOF //
