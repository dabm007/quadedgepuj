#ifndef CGAL_QUADEDGEDS_MIN_ITEMS_H
#define CGAL_QUADEDGEDS_MIN_ITEMS_H 1
#include <QuadedgeDS_vertex_min_base.h>
#include <QuadedgeDS_quadedge_min_base.h>
#include <QuadedgeDS_face_min_base.h>

namespace CGAL_UPJ {

class QuadedgeDS_min_items {
public:
    template < class Refs, class Traits>
    struct Vertex_wrapper {
        typedef QuadedgeDS_vertex_min_base< Refs>   Vertex;
    };
    template < class Refs, class Traits>
    struct Quadedge_wrapper {
        typedef QuadedgeDS_quadedge_min_base< Refs> Quadedge;
    };
    template < class Refs, class Traits>
    struct Face_wrapper {
        typedef QuadedgeDS_face_min_base< Refs>     Face;
    };
};

} //namespace CGAL
#endif // CGAL_QUADEDGEDS_MIN_ITEMS_H //
// EOF //

