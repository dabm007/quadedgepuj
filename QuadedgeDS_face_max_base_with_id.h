#ifndef CGAL_QUADEDGEDS_FACE_MAX_BASE_WITH_ID_H
#define CGAL_QUADEDGEDS_FACE_MAX_BASE_WITH_ID_H 1

#include <QuadedgeDS_face_base.h>

namespace CGAL_UPJ {

template < class Refs, class Pln, class ID>
class QuadedgeDS_face_max_base_with_id : public QuadedgeDS_face_base< Refs, Tag_true, Pln>
{
public:
    
    typedef QuadedgeDS_face_base< Refs, Tag_true, Pln> Base ;
    
    typedef ID size_type ;
    
private:

    size_type mID ;
    
public:

    QuadedgeDS_face_max_base_with_id() : mID ( size_type(-1) ) {}
    QuadedgeDS_face_max_base_with_id( Pln const& p) : Base(p), mID ( size_type(-1) ) {}
    QuadedgeDS_face_max_base_with_id( Pln const& p, size_type i ) : Base(p), mID (i) {}
    
    size_type&       id()       { return mID; }
    size_type const& id() const { return mID; }
};

} //namespace CGAL

#endif // CGAL_QUADEDGEDS_FACE_MAX_BASE_WITH_ID_H //
// EOF //
