#ifndef CGAL_QUADEDGEDS_VECTOR_H
#define CGAL_QUADEDGEDS_VECTOR_H 1

// Define this if QuadedgeDS_vector is based on CGAL internal vector.
#define CGAL__QUADEDGEDS_USE_INTERNAL_VECTOR 1

#include <CGAL/basic.h>
#include <CGAL/memory.h>
#include <QuadedgeDS_items_decorator.h>
#include <algorithm>
#include <vector>
#include <map>
#include <cstddef>

#ifdef CGAL__QUADEDGEDS_USE_INTERNAL_VECTOR
#include <CGAL/vector.h>
#else
#include <QuadedgeDS_iterator_adaptor.h>
#endif

namespace CGAL_UPJ {


template < class Traits_, class QuadedgeDSItems, class Alloc>
class QuadedgeDS_vector_types {
public:
    typedef QuadedgeDS_vector_types<Traits_,QuadedgeDSItems,Alloc> Self;
    typedef Traits_                                    Traits;
    typedef QuadedgeDSItems                            Items;
    typedef Alloc                                      Allocator;
    typedef Alloc                                      allocator_type;

    typedef typename Items::template Vertex_wrapper<Self,Traits>   
                                                       Vertex_wrapper;
    typedef typename Items::template Quadedge_wrapper<Self,Traits>
                                                       Quadedge_wrapper;
    typedef typename Items::template Face_wrapper<Self,Traits>
                                                       Face_wrapper;

    typedef typename Vertex_wrapper::Vertex            Vertex;
    typedef typename Quadedge_wrapper::Quadedge        Quadedge;
    typedef typename Face_wrapper::Face                Face;

    typedef typename Allocator::template rebind< Vertex> Vertex_alloc_rebind;
    typedef typename Vertex_alloc_rebind::other        Vertex_allocator;
    typedef typename Allocator::template rebind< Quadedge>
                                                       Quadedge_alloc_rebind;
    typedef typename Quadedge_alloc_rebind::other      Quadedge_allocator;
    typedef typename Allocator::template rebind< Face> Face_alloc_rebind;
    typedef typename Face_alloc_rebind::other          Face_allocator;

#ifdef CGAL__QUADEDGEDS_USE_INTERNAL_VECTOR
    typedef internal::vector<Vertex, Vertex_allocator>    Vertex_vector;
    typedef typename Vertex_vector::iterator           Vertex_I;
    typedef typename Vertex_vector::const_iterator     Vertex_CI;
    typedef typename Vertex_vector::iterator           Vertex_iterator;
    typedef typename Vertex_vector::const_iterator     Vertex_const_iterator;

    typedef internal::vector<Quadedge, Quadedge_allocator>  Quadedge_vector;
    typedef typename Quadedge_vector::iterator         Quadedge_I;
    typedef typename Quadedge_vector::const_iterator   Quadedge_CI;
    typedef typename Quadedge_vector::iterator         Quadedge_iterator;
    typedef typename Quadedge_vector::const_iterator   Quadedge_const_iterator;

    typedef internal::vector<Face, Face_allocator>        Face_vector;
    typedef typename Face_vector::iterator             Face_I;
    typedef typename Face_vector::const_iterator       Face_CI;
    typedef typename Face_vector::iterator             Face_iterator;
    typedef typename Face_vector::const_iterator       Face_const_iterator;
#else // CGAL__QUADEDGEDS_USE_INTERNAL_VECTOR //
    typedef std::vector<Vertex, Vertex_allocator>      Vertex_vector;
    typedef typename Vertex_vector::iterator           Vertex_I;
    typedef typename Vertex_vector::const_iterator     Vertex_CI;
    typedef QuadedgeDS_iterator_adaptor<Vertex_I>      Vertex_iterator;
    typedef QuadedgeDS_iterator_adaptor<Vertex_CI>     Vertex_const_iterator;

    typedef std::vector<Quadedge, Quadedge_allocator>  Quadedge_vector;
    typedef typename Quadedge_vector::iterator         Quadedge_I;
    typedef typename Quadedge_vector::const_iterator   Quadedge_CI;
    typedef QuadedgeDS_iterator_adaptor<Quadedge_I>    Quadedge_iterator;
    typedef QuadedgeDS_iterator_adaptor<Quadedge_CI>   Quadedge_const_iterator;

    typedef std::vector<Face, Face_allocator>          Face_vector;
    typedef typename Face_vector::iterator             Face_I;
    typedef typename Face_vector::const_iterator       Face_CI;
    typedef QuadedgeDS_iterator_adaptor<Face_I>        Face_iterator;
    typedef QuadedgeDS_iterator_adaptor<Face_CI>       Face_const_iterator;
#endif // CGAL__QUADEDGEDS_USE_INTERNAL_VECTOR //

    typedef Vertex_iterator                            Vertex_handle;
    typedef Vertex_const_iterator                      Vertex_const_handle;
    typedef Quadedge_iterator                          Quadedge_handle;
    typedef Quadedge_const_iterator                    Quadedge_const_handle;
    typedef Face_iterator                              Face_handle;
    typedef Face_const_iterator                        Face_const_handle;

    typedef typename Quadedge_vector::size_type        size_type;
    typedef typename Quadedge_vector::difference_type  difference_type;
    typedef std::random_access_iterator_tag            iterator_category;

    static inline Vertex_handle vertex_handle( Vertex* v) {
        return Vertex_handle( Vertex_I(v));
    }
    static inline Vertex_const_handle vertex_handle( const Vertex* v) {
        return Vertex_const_handle( Vertex_CI(v));
    }
    static inline Quadedge_handle quadedge_handle( Quadedge* h) {
        return Quadedge_handle( Quadedge_I(h));
    }
    static inline Quadedge_const_handle quadedge_handle( const Quadedge* h) {
        return Quadedge_const_handle( Quadedge_CI(h));
    }
    static inline Face_handle face_handle( Face* f) {
        return Face_handle( Face_I(f));
    }
    static inline Face_const_handle face_handle( const Face* f) {
        return Face_const_handle( Face_CI(f));
    }
};


template < class Traits_, class QuadedgeDSItems, 
           class Alloc = CGAL_ALLOCATOR(int)>
class QuadedgeDS_vector
    : public QuadedgeDS_vector_types<Traits_, QuadedgeDSItems, Alloc> {
public:
    typedef QuadedgeDS_vector<Traits_,QuadedgeDSItems,Alloc> Self;
    typedef QuadedgeDS_vector_types<Traits_, QuadedgeDSItems, Alloc> Types;
    typedef typename Types::Traits                     Traits;
    typedef typename Types::Items                      Items;
    typedef typename Types::Allocator                  Allocator;
    typedef typename Types::allocator_type             allocator_type;

    typedef typename Types::Vertex                     Vertex;
    typedef typename Types::Quadedge                   Quadedge;
    typedef typename Types::Face                       Face;

    typedef typename Types::Vertex_vector              Vertex_vector;
    typedef typename Types::Vertex_handle              Vertex_handle;
    typedef typename Types::Vertex_const_handle        Vertex_const_handle;
    typedef typename Types::Vertex_iterator            Vertex_iterator;
    typedef typename Types::Vertex_const_iterator      Vertex_const_iterator;
    typedef typename Types::Vertex_I                   Vertex_I;
    typedef typename Types::Vertex_CI                  Vertex_CI;

    typedef typename Types::Quadedge_vector            Quadedge_vector;
    typedef typename Types::Quadedge_handle            Quadedge_handle;
    typedef typename Types::Quadedge_const_handle      Quadedge_const_handle;
    typedef typename Types::Quadedge_iterator          Quadedge_iterator;
    typedef typename Types::Quadedge_const_iterator    Quadedge_const_iterator;
    typedef typename Types::Quadedge_I                 Quadedge_I;
    typedef typename Types::Quadedge_CI                Quadedge_CI;

    typedef typename Types::Face_vector                Face_vector;
    typedef typename Types::Face_handle                Face_handle;
    typedef typename Types::Face_const_handle          Face_const_handle;
    typedef typename Types::Face_iterator              Face_iterator;
    typedef typename Types::Face_const_iterator        Face_const_iterator;
    typedef typename Types::Face_I                     Face_I;
    typedef typename Types::Face_CI                    Face_CI;

    typedef typename Types::size_type                  size_type;
    typedef typename Types::difference_type            difference_type;
    typedef typename Types::iterator_category          iterator_category;

    typedef Tag_false                                  Supports_removal;

    typedef typename Vertex::Supports_vertex_quadedge Supports_vertex_quadedge;
    typedef typename Quadedge::Supports_quadedge_prev Supports_quadedge_prev;
    typedef typename Quadedge::Supports_quadedge_vertex
                                                      Supports_quadedge_vertex;
    typedef typename Quadedge::Supports_quadedge_face
                                                      Supports_quadedge_face;
    typedef typename Face::Supports_face_quadedge     Supports_face_quadedge;

protected:
    typedef typename Vertex::Base                      VBase;
    typedef typename Quadedge::Base                    QBase;
    typedef typename Quadedge::Base_base               QBase_base;
    typedef typename Face::Base                        FBase;

    Vertex_vector      vertices;
    Quadedge_vector    quadedges;
    Face_vector        faces;

    size_type          nb_border_quadedges;
    size_type          nb_border_edges;
    Quadedge_iterator  border_quadedges;

#ifdef CGAL__HALFEDGEDS_USE_INTERNAL_VECTOR
    Vertex_I    get_v_iter( const Vertex_I&  i)   const { return i; }
    Vertex_CI   get_v_iter( const Vertex_CI& i)   const { return i; }
    Quadedge_I  get_h_iter( const Quadedge_I&  i) const { return i; }
    Quadedge_CI get_h_iter( const Quadedge_CI& i) const { return i; }
    Face_I      get_f_iter( const Face_I&  i)     const { return i; }
    Face_CI     get_f_iter( const Face_CI& i)     const { return i; }
#else // CGAL__QUADEDGEDS_USE_INTERNAL_VECTOR //
    Vertex_I    get_v_iter( const Vertex_iterator&  i) const {
        return i.iterator();
    }
    Vertex_CI   get_v_iter( const Vertex_const_iterator& i)   const {
        return i.iterator();
    }
    Quadedge_I  get_h_iter( const Quadedge_iterator&  i) const {
        return i.iterator();
    }
    Quadedge_CI get_h_iter( const Quadedge_const_iterator& i) const {
        return i.iterator();
    }
    Face_I      get_f_iter( const Face_iterator&  i)     const {
        return i.iterator();
    }
    Face_CI     get_f_iter( const Face_const_iterator& i)     const {
        return i.iterator();
    }
#endif // CGAL__QUADEDGEDS_USE_INTERNAL_VECTOR //

// CREATION

private:
#define CGAL__V_UPDATE(v) (((v) == Vertex_handle()) ? (v) : \
                           (v_new + ( Vertex_CI   (get_v_iter(v)) - v_old)))
#define CGAL__H_UPDATE(h) (((h) == Quadedge_handle()) ? (h) : \
                           (h_new + ( Quadedge_CI (get_h_iter(h)) - h_old)))
#define CGAL__F_UPDATE(f) (((f) == Face_handle()) ? (f) : \
                           (f_new + ( Face_CI     (get_f_iter(f)) - f_old)))

    void pointer_update( Vertex_CI v_old, Quadedge_CI h_old, Face_CI f_old) {
        // Update own pointers assuming that they lived previously
        // in a quadedge data structure with vector starting addresses
        // as given as parameters v_old, h_old, f_old.
        QuadedgeDS_items_decorator<Self> D;
        Vertex_iterator   v_new = vertices.begin();
        Quadedge_iterator h_new = quadedge.begin();
        Face_iterator     f_new = faces.begin();
        for (Quadedge_iterator h = quadedge_begin();h != quadedges_end();++h){
            h->QBase::set_next( CGAL__H_UPDATE( h->next()));
            h->QBase_base::set_opposite( CGAL__H_UPDATE( h->opposite()));
            D.set_prev(   h, CGAL__H_UPDATE( D.get_prev(h)));
            D.set_vertex( h, CGAL__V_UPDATE( D.get_vertex(h)));
            D.set_face(   h, CGAL__F_UPDATE( D.get_face(h)));
        }
        border_quadedges = CGAL__H_UPDATE( border_quadedges);
        if (check_tag( Supports_vertex_quadedge())) {
            for (Vertex_iterator v = vertices_begin();v != vertices_end();++v){
                D.set_vertex_quadedge(v, CGAL__H_UPDATE(
                    D.get_vertex_quadedge(v)));
            }
        }
        if (check_tag( Supports_face_quadedge())) {
            for ( Face_iterator f = faces_begin(); f != faces_end(); ++f) {
                D.set_face_quadedge(f,CGAL__H_UPDATE( D.get_face_quadedge(f)));
            }
        }
    }
#undef CGAL__V_UPDATE
#undef CGAL__H_UPDATE
#undef CGAL__F_UPDATE

public:

    QuadedgeDS_vector()
        : nb_border_quadedges(0), nb_border_edges(0) {}
        // empty quadedge data structure.

    QuadedgeDS_vector( size_type v, size_type h, size_type f)
        : nb_border_quadedges(0), nb_border_edges(0) {
        // quadedge data structure with storage reserved for v vertices, h
        // quadedges, and f faces. The reservation sizes are a hint for
        // optimizing storage allocation. They are not used here.
        vertices.reserve(v);
        quadedges.reserve(h);
        faces.reserve(f);
    }

    QuadedgeDS_vector( const Self& qds)
    :  vertices( qds.vertices),
       quadedges( qds.quadedges),
       faces( qds.faces),
       nb_border_quadedges( qds.nb_border_quadedges),
       nb_border_edges( qds.nb_border_edges),
       border_quadedges( qds.border_quadedges)
    {
        pointer_update( qds.vertices.begin(),
                        qds.quadedges.begin(),
                        qds.faces.begin());
    }

    Self& operator=( const Self& qds)  {
        if ( this != &qds) {
            clear();
            vertices            = qds.vertices;
            quadedges           = qds.quadedges;
            faces               = qds.faces;
            nb_border_quadedges = qds.nb_border_quadedges;
            nb_border_edges     = qds.nb_border_edges;
            border_quadedges    = qds.border_quadedges;
            pointer_update( qds.vertices.begin(),
                            qds.halfedges.begin(),
                            qds.faces.begin());
        }
        return *this;
    }

    void reserve( size_type v, size_type h, size_type f) {
        // reserve storage for v vertices, h quadedges, and f faces. The
        // reservation sizes are a hint for optimizing storage allocation.
        // If the `capacity' is already greater than the requested size
        // nothing happens. If the `capacity' changes all iterators and
        // circulators invalidates. Function is void here.
        if ( (check_tag( Supports_quadedge_vertex())
                && v > capacity_of_vertices())
             || h > capacity_of_quadedges()
             || (check_tag( Supports_quadedge_face())
                && f > capacity_of_faces())) {
            Vertex_CI   v_old = vertices.begin();
            Quadedge_CI h_old = quadedges.begin();
            Face_CI     f_old = faces.begin();
            if ( check_tag( Supports_quadedge_vertex()))
                vertices.reserve(v);
            quadedges.reserve(h);
            if ( check_tag( Supports_quadedge_face()))
                faces.reserve(f);
            pointer_update( v_old, h_old, f_old);
        }
    }

// Access Member Functions

    allocator_type  get_allocator() const { return allocator_type(); }

    size_type size_of_vertices() const  { return vertices.size();}
    size_type size_of_quadedges() const { return quadedges.size();}
        // number of all quadedges (including border quadedges).
    size_type size_of_faces() const     { return faces.size();}

    size_type capacity_of_vertices() const    { return vertices.capacity();}
    size_type capacity_of_quadedges() const   { return quadedges.capacity();}
    size_type capacity_of_faces() const       { return faces.capacity();}

    std::size_t bytes() const {
        return sizeof(Self)
               + vertices.size()  * sizeof( Vertex)
               + quadedges.size() * sizeof( Quadedge)
               + faces.size()     * sizeof( Face);
    }
    std::size_t bytes_reserved() const {
        return sizeof(Self)
               + vertices.capacity()  * sizeof( Vertex)
               + quadedges.capacity() * sizeof( Quadedge)
               + faces.capacity()     * sizeof( Face);
    }

    Vertex_iterator   vertices_begin()   { return vertices.begin();}
    Vertex_iterator   vertices_end()     { return vertices.end();}
    Quadedge_iterator quadedges_begin()  { return quadedges.begin();}
    Quadedge_iterator quadedges_end()    { return quadedges.end();}
    Face_iterator     faces_begin()      { return faces.begin();}
    Face_iterator     faces_end()        { return faces.end();}

    // The constant iterators and circulators.

    Vertex_const_iterator   vertices_begin()  const{ return vertices.begin();}
    Vertex_const_iterator   vertices_end()    const{ return vertices.end();}
    Quadedge_const_iterator quadedges_begin() const{ return quadedges.begin();}
    Quadedge_const_iterator quadedges_end()   const{ return quadedges.end();}
    Face_const_iterator     faces_begin()     const{ return faces.begin();}
    Face_const_iterator     faces_end()       const{ return faces.end();}

// Insertion
//
// The following operations simply allocate a new element of that type.
// Quadedge are always allocated in pairs of opposite halfedges. The
// opposite pointers are automatically set.

    Vertex_handle vertices_push_back( const Vertex& v) {
        CGAL_precondition( 1+size_of_vertices() <= capacity_of_vertices());
        vertices.push_back(v);
        Vertex_handle vv = vertices.end();
        return --vv;
    }

    Quadedge_handle edges_push_back( const Quadedge& h, const Quadedge& g) {
        // creates a new pair of opposite border halfedges.
        CGAL_precondition( 1 + size_of_quadedges() < capacity_of_quadedges());
        quadedges.push_back(h);
        Quadedge_handle hh = quadedges.end();
        --hh;
        quadedges.push_back(g);
        Quadedge_handle gg = quadedges.end();
        --gg;
        CGAL_assertion( hh + 1 == gg);
        CGAL_assertion( (char*)(&*gg) - (char*)(&*hh) == sizeof( Quadedge));
        hh->QBase_base::set_opposite(gg);
        gg->QBase_base::set_opposite(hh);
        return hh;
    }

    Quadedge_handle edges_push_back( const Quadedge& h) {
        CGAL_precondition( h.opposite() != Quadedge_const_handle());
        return edges_push_back( h, *(h.opposite()));
    }

    Face_handle faces_push_back( const Face& f) {
        CGAL_precondition( 1+size_of_faces() <= capacity_of_faces());
        faces.push_back(f);
        Face_handle ff = faces.end();
        return --ff;
    }


// Removal
//
// The following operations erase an element referenced by a handle.
// Quadedge are always deallocated in pairs of opposite quadedges. Erase
// of single elements is optional. The deletion of all is mandatory.

    void vertices_pop_back() { vertices.pop_back(); }
    void edges_pop_back()    {
        CGAL_precondition(( quadedges_end()-1)->opposite() ==
                          ( quadedges_end()-2));
        quadedges.pop_back();
        quadedges.pop_back();
    }
    void faces_pop_back()    { faces.pop_back(); }

    void vertices_clear() { vertices.erase( vertices.begin(), vertices.end());}
    void edges_clear() {
        quadedges.erase( quadedges.begin(), quadedges.end());
        nb_border_quadedges = 0;
        nb_border_edges = 0;
        border_quadedges = Quadedge_handle();
    }
    void faces_clear() { faces.erase( faces.begin(), faces.end()); }
    void clear() {
        vertices_clear();
        edges_clear();
        faces_clear();
    }

// Special Operations on Polyhedral Surfaces
protected:
    // Update operation used in pointer_update(...).
    void update_opposite( Quadedge_I h) {
        Quadedge_I g = h + 1;
        h->QBase_base::set_opposite(g);
        g->QBase_base::set_opposite(h);
    }

// Operations with Border Quadedge
public:
    size_type size_of_border_quadedges() const { return nb_border_quadedges;}
        // number of border quadedges. An edge with no incident face
        // counts as two border quadedges. Precondition: `normalize_border()'
        // has been called and no quadedge insertion or removal and no
        // change in border status of the quadedges have occured since
        // then.

    size_type size_of_border_edges() const { return nb_border_edges;}
        // number of border edges. If `size_of_border_edges() ==
        // size_of_border_quadedges()' all border edges are incident to a
        // face on one side and to a hole on the other side.
        // Precondition: `normalize_border()' has been called and no
        // quadedge insertion or removal and no change in border status of
        // the quadedges have occured since then.

    Quadedge_iterator border_quadedges_begin() {
        // quadedge iterator starting with the border edges. The range [
        // `quadedges_begin(), border_quadedges_begin()') denotes all
        // non-border edges. The range [`border_quadedges_begin(),
        // quadedges_end()') denotes all border edges. Precondition:
        // `normalize_border()' has been called and no halfedge insertion
        // or removal and no change in border status of the quadedges have
        // occured since then.
        return border_quadedges;
    }

    Quadedge_const_iterator border_quadedges_begin() const {
        return border_quadedges;
    }

    void normalize_border() {
        // sorts quadedges such that the non-border edges precedes the
        // border edges. For each border edge that is incident to a face
        // the quadedge iterator will reference the quadedge incident to
        // the face right before the quadedge incident to the hole.
        nb_border_quadedges = 0;
        nb_border_edges = 0;
        border_quadedges = quadedges_end();
        // Lets run one partition step over the array of quadedges.
        // First find a pivot -- that means a border edge.
        Quadedge_I ll = quadedges.begin();
        while ( ll != quadedges.end() && (! ll->is_border()) &&
                (! ll->opposite()->is_border() ))
            ++ ++ll;
        if ( ll == quadedges.end()) // Done. No border edges found.
            return;

        // An array of pointers to update the changed halfedge pointers.
        typedef typename Allocator::template rebind< Quadedge_I>
                                                      HI_alloc_rebind;
        typedef typename HI_alloc_rebind::other       HI_allocator;

        typedef std::vector<Quadedge_I, HI_allocator> HVector;
        typedef typename HVector::iterator Hiterator;
        HVector hvector;
        // Initialize it.
        hvector.reserve( quadedges.size());
        for ( Quadedge_I i = quadedges.begin(); i != quadedges.end(); ++i) {
            hvector.push_back(i);
        }
        Hiterator llhv = hvector.begin() + (ll-quadedges.begin());
        // Start with the partitioning.
        Quadedge_I rr = quadedges.end();
        -- --rr;
        Hiterator rrhv = hvector.end();
        -- --rrhv;
        // The comments proove the invariant of the partitioning step.
        // Note that + 1 or - 1 denotes plus one edge or minus one edge,
        // so they mean actually + 2 and - 2.
                              // Pivot is in *ll
                              // Elements in [rr+1..end) >= pivot (border)
                              // Elements in [begin..ll) <  pivot (non border)
        while (ll < rr) {
                              // Pivot is in *ll, ll <= rr.
            while ( rr > ll && (rr->is_border() 
                              || rr->opposite()->is_border())) {
                if ( ! rr->opposite()->is_border()) {
                    CGAL_assertion( rr + 1 == get_h_iter(rr->opposite()));
                    std::swap( *rr, *(rr+1));
                    update_opposite( rr);
                    std::swap( *rrhv, *(rrhv+1));
                }
                -- --rr;
                -- --rrhv;
            }
                              // Elements in [rr+1..end) >= pivot (border)
                              // *rr <= pivot, ll <= rr.
            CGAL_assertion( rr + 1 == get_h_iter( rr->opposite()));
            CGAL_assertion( ll + 1 == get_h_iter( ll->opposite()));
            std::swap( *(ll+1), *(rr+1));
            std::swap( *ll, *rr);
            update_opposite( ll);
            update_opposite( rr);
            std::swap( *(llhv+1), *(rrhv+1));
            std::swap( *llhv, *rrhv);
                              // Elements in [begin..ll) < pivot
                              // Pivot is in *rr, ll <= rr.
            while ( !ll->is_border() && ! ll->opposite()->is_border()) {
                ++ ++ll;
                ++ ++llhv;
            }
                              // Elements in [begin..ll) < pivot
                              // *ll >= pivot
                              // ll <= rr (since *rr is pivot.)
            CGAL_assertion( ll <= rr);
            CGAL_assertion( llhv <= rrhv);
            CGAL_assertion( rr + 1 == get_h_iter( rr->opposite()));
            CGAL_assertion( ll + 1 == get_h_iter( ll->opposite()));
            std::swap( *(ll+1), *(rr+1));
            std::swap( *ll, *rr);
            update_opposite( ll);
            update_opposite( rr);
            std::swap( *(llhv+1), *(rrhv+1));
            std::swap( *llhv, *rrhv);
            if ( ! rr->opposite()->is_border()) {
                CGAL_assertion( rr + 1 == get_h_iter( rr->opposite()));
                std::swap( *rr, *(rr+1));
                update_opposite( rr);
                std::swap( *rrhv, *(rrhv+1));
            }

            // This guard is needed here because, rr==ll==begin, might be true
            // at this point, causing the decrement to result in undefined
            // behaviour.
            // [Fernando Cacciola]
            if ( ll < rr )
            {
              -- --rr;
              -- --rrhv;
            }
                              // Elements in [rr+1..end) >= pivot
                              // Pivot is in *ll
        }
        CGAL_assertion( llhv >= rrhv);
                              // rr + 1 >= ll >= rr
                              // Elements in [rr+1..end) >= pivot
                              // Elemente in [begin..ll) <  pivot
                              // Pivot is in a[ll]
        if ( ll == rr) {
            // Check for the possibly missed swap.
            if ( rr->is_border() && ! rr->opposite()->is_border()) {
                CGAL_assertion( rr + 1 == get_h_iter (rr->opposite()));
                std::swap( *rr, *(rr+1));
                update_opposite( rr);
                std::swap( *rrhv, *(rrhv+1));
            }
        }
        CGAL_assertion( ll->opposite()->is_border());
        CGAL_assertion( ll == quadedges.begin() || ! (ll-2)->is_border());
        CGAL_assertion( ll == quadedges.begin() || ! (ll-1)->is_border());
        border_quadedges = ll;
        nb_border_edges = (quadedges.end() - ll) / 2;
        nb_border_quadedges = 0;

        HVector inv_vector( quadedges.size());
        // Initialize inverse index.
        for ( Hiterator k = hvector.begin(); k != hvector.end(); ++k){
            inv_vector[*k - quadedges.begin()] =
                quadedges.begin() + (k - hvector.begin());
        }
        // Update quadedge pointers.
        QuadedgeDS_items_decorator<Self> D;
        for (Quadedge_iterator h = quadedges_begin();h != quadedges_end();++h){
            h->QBase::set_next( inv_vector[ h->next() - quadedges_begin()]);
            D.set_vertex_quadedge(h);
            if ( D.get_prev( h) == Quadedge_iterator())
                D.set_prev( h, Quadedge_iterator());
            else
                D.set_prev( h, inv_vector[ D.get_prev(h) - quadedges_begin()]);
            if ( h->is_border())
                nb_border_quadedges++;
            else
                D.set_face_quadedge(h);
        }
    }
};


} //namespace CGAL
#endif // CGAL_QUADEDGEDS_VECTOR_H //
// EOF //

