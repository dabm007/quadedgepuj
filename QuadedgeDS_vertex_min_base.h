#ifndef CGAL_QUADEDGEDS_VERTEX_MIN_BASE_H
#define CGAL_QUADEDGEDS_VERTEX_MIN_BASE_H 1

#include <CGAL/basic.h>

namespace CGAL_UPJ {

template < class Refs>
class QuadedgeDS_vertex_min_base {
public:
    typedef Refs                                 QuadedgeDS;
    typedef QuadedgeDS_vertex_min_base< Refs>    Base;
    typedef Tag_false                            Supports_vertex_quadedge;
    typedef Tag_false                            Supports_vertex_point;
    typedef typename Refs::Vertex_handle         Vertex_handle;
    typedef typename Refs::Vertex_const_handle   Vertex_const_handle;
    typedef typename Refs::Quadedge_handle       Quadedge_handle;
    typedef typename Refs::Quadedge_const_handle Quadedge_const_handle;
    typedef typename Refs::Face_handle           Face_handle;
    typedef typename Refs::Face_const_handle     Face_const_handle;
    typedef typename Refs::Quadedge              Quadedge;
    typedef typename Refs::Face                  Face;
};

} //namespace CGAL

#endif // CGAL_QUADEDGEDS_VERTEX_MIN_BASE_H //
// EOF //

