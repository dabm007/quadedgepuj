#ifndef CGAL_QUADEDGEDS_FACE_BASE_H
#define CGAL_QUADEDGEDS_FACE_BASE_H 1

#include <CGAL/basic.h>

namespace CGAL_UPJ {

// We use Tag_false to indicate that no plane type is provided.

template < class Refs, class T = Tag_true, class Pln = Tag_false>
class QuadedgeDS_face_base;

template < class Refs >
class QuadedgeDS_face_base< Refs, Tag_false, Tag_false> {
public:
    typedef Refs                                 QuadedgeDS;
    typedef QuadedgeDS_face_base< Refs, Tag_false, Tag_false>  Base;
    typedef Tag_false                            Supports_face_quadedge;
    typedef typename Refs::Vertex_handle         Vertex_handle;
    typedef typename Refs::Vertex_const_handle   Vertex_const_handle;
    typedef typename Refs::Quadedge_handle       Quadedge_handle;
    typedef typename Refs::Quadedge_const_handle Quadedge_const_handle;
    typedef typename Refs::Face_handle           Face_handle;
    typedef typename Refs::Face_const_handle     Face_const_handle;
    typedef typename Refs::Vertex                Vertex;
    typedef typename Refs::Quadedge              Quadedge;
    // Additional tags required by Polyhedron.
    typedef Tag_false                            Supports_face_plane;
    struct Plane_not_supported {};
    typedef Plane_not_supported                  Plane;
    // No longer required.
    // typedef Tag_false                            Supports_face_normal;
};

template < class Refs >
class QuadedgeDS_face_base< Refs, Tag_true, Tag_false> {
public:
    typedef Refs                                 QuadedgeDS;
    typedef QuadedgeDS_face_base< Refs, Tag_true, Tag_false>   Base;
    typedef Tag_true                             Supports_face_quadedge;
    typedef typename Refs::Vertex_handle         Vertex_handle;
    typedef typename Refs::Vertex_const_handle   Vertex_const_handle;
    typedef typename Refs::Quadedge_handle       Quadedge_handle;
    typedef typename Refs::Quadedge_const_handle Quadedge_const_handle;
    typedef typename Refs::Face_handle           Face_handle;
    typedef typename Refs::Face_const_handle     Face_const_handle;
    typedef typename Refs::Vertex                Vertex;
    typedef typename Refs::Quadedge              Quadedge;
    // Additional tags required by Polyhedron.
    typedef Tag_false                            Supports_face_plane;
    struct Plane_not_supported {};
    typedef Plane_not_supported                  Plane;
    // No longer required.
    //typedef Tag_false                            Supports_face_normal;
private:
    Quadedge_handle hdg;
public:
    Quadedge_handle       quadedge()                        { return hdg; }
    Quadedge_const_handle quadedge() const                  { return hdg; }
    void                  set_quadedge( Quadedge_handle h)  { hdg = h; }
};

template < class Refs, class Pln >
class QuadedgeDS_face_base< Refs, Tag_false, Pln> {
public:
    typedef Refs                                 QuadedgeDS;
    typedef QuadedgeDS_face_base< Refs, Tag_false, Pln>     Base;
    typedef Tag_false                            Supports_face_quadedge;
    typedef typename Refs::Vertex_handle         Vertex_handle;
    typedef typename Refs::Vertex_const_handle   Vertex_const_handle;
    typedef typename Refs::Quadedge_handle       Quadedge_handle;
    typedef typename Refs::Quadedge_const_handle Quadedge_const_handle;
    typedef typename Refs::Face_handle           Face_handle;
    typedef typename Refs::Face_const_handle     Face_const_handle;
    typedef typename Refs::Vertex                Vertex;
    typedef typename Refs::Quadedge              Quadedge;
    // Additional tags and types required by Polyhedron.
    typedef Tag_true                             Supports_face_plane;
    typedef Pln                                  Plane;
    // No longer required.
    //typedef Tag_true                             Supports_face_normal;
    //typedef Trts                                 Traits;
    //typedef typename Traits::Normal              Normal;
    //typedef typename Traits::Plane               Plane;
private:
    Plane  pln;
public:
    QuadedgeDS_face_base() {}
    QuadedgeDS_face_base( const Plane& g) : pln(g) {}
    Plane&                plane()                           { return pln; }
    const Plane&          plane() const                     { return pln; }
    // No longer required.
    // Normal              normal() const { return pln.orthogonal_vector();}
};

template < class Refs, class Pln >
class QuadedgeDS_face_base< Refs, Tag_true, Pln> {
public:
    typedef Refs                                 QuadedgeDS;
    typedef QuadedgeDS_face_base< Refs, Tag_true, Pln>      Base;
    typedef Tag_true                             Supports_face_quadedge;
    typedef typename Refs::Vertex_handle         Vertex_handle;
    typedef typename Refs::Vertex_const_handle   Vertex_const_handle;
    typedef typename Refs::Quadedge_handle       Quadedge_handle;
    typedef typename Refs::Quadedge_const_handle Quadedge_const_handle;
    typedef typename Refs::Face_handle           Face_handle;
    typedef typename Refs::Face_const_handle     Face_const_handle;
    typedef typename Refs::Vertex                Vertex;
    typedef typename Refs::Quadedge              Quadedge;
    // Additional tags and types required by Polyhedron.
    typedef Tag_true                             Supports_face_plane;
    typedef Pln                                  Plane;
    // No longer required.
    //typedef Tag_true                             Supports_face_normal;
    //typedef Trts                                 Traits;
    //typedef typename Traits::Normal              Normal;
    //typedef typename Traits::Plane               Plane;
private:
    Quadedge_handle hdg;
    Plane           pln;
public:
    QuadedgeDS_face_base() {}
    QuadedgeDS_face_base( const Plane& g) : pln(g) {}
    Quadedge_handle       quadedge()                        { return hdg; }
    Quadedge_const_handle quadedge() const                  { return hdg; }
    void                  set_quadedge( Quadedge_handle h)  { hdg = h; }
    Plane&                plane()                           { return pln; }
    const Plane&          plane() const                     { return pln; }
    // No longer required.
    //Normal                normal() const { return pln.orthogonal_vector();}
};

} //namespace CGAL

#endif // CGAL_QUADEDGEDS_FACE_BASE_H //
// EOF //
