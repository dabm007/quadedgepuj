#ifndef CGAL_QUADEDGEDS_VERTEX_BASE_H
#define CGAL_QUADEDGEDS_VERTEX_BASE_H 1

#include <CGAL/basic.h>

typedef CGAL::Tag_true	Tag_true;
typedef CGAL::Tag_false	Tag_false;

namespace CGAL_UPJ {

// We use Tag_false to indicate that no point type is provided.

template < class Refs, class T = Tag_true, class P = Tag_false>
class QuadedgeDS_vertex_base;

template < class Refs >
class QuadedgeDS_vertex_base< Refs, Tag_false, Tag_false> {
public:
    typedef Refs                                 QuadedgeDS;
    typedef QuadedgeDS_vertex_base< Refs, Tag_false, Tag_false>  Base;
    typedef Tag_false                            Supports_vertex_quadedge;
    typedef Tag_false                            Supports_vertex_point;
    typedef typename Refs::Vertex_handle         Vertex_handle;
    typedef typename Refs::Vertex_const_handle   Vertex_const_handle;
    typedef typename Refs::Quadedge_handle       Quadedge_handle;
    typedef typename Refs::Quadedge_const_handle Quadedge_const_handle;
    typedef typename Refs::Face_handle           Face_handle;
    typedef typename Refs::Face_const_handle     Face_const_handle;
    typedef typename Refs::Quadedge              Quadedge;
    typedef typename Refs::Face                  Face;
};

template < class Refs>
class QuadedgeDS_vertex_base< Refs, Tag_true, Tag_false> {
public:
    typedef Refs                                 QuadedgeDS;
    typedef QuadedgeDS_vertex_base< Refs, Tag_true, Tag_false>   Base;
    typedef Tag_true                             Supports_vertex_quadedge;
    typedef Tag_false                            Supports_vertex_point;
    typedef typename Refs::Vertex_handle         Vertex_handle;
    typedef typename Refs::Vertex_const_handle   Vertex_const_handle;
    typedef typename Refs::Quadedge_handle       Quadedge_handle;
    typedef typename Refs::Quadedge_const_handle Quadedge_const_handle;
    typedef typename Refs::Face_handle           Face_handle;
    typedef typename Refs::Face_const_handle     Face_const_handle;
    typedef typename Refs::Quadedge              Quadedge;
    typedef typename Refs::Face                  Face;
private:
    Quadedge_handle qdg;
public:
    Quadedge_handle       quadedge()                        { return qdg; }
    Quadedge_const_handle quadedge() const                  { return qdg; }
    void                  set_quadedge( Quadedge_handle q)  { qdg = q; }
};

template < class Refs, class P>
class QuadedgeDS_vertex_base< Refs, Tag_false, P> {
public:
    typedef Refs                                 QuadedgeDS;
    typedef QuadedgeDS_vertex_base< Refs, Tag_false, P>     Base;
    typedef Tag_false                            Supports_vertex_quadedge;
    typedef Tag_true                             Supports_vertex_point;
    typedef P                                    Point;
    typedef typename Refs::Vertex_handle         Vertex_handle;
    typedef typename Refs::Vertex_const_handle   Vertex_const_handle;
    typedef typename Refs::Quadedge_handle       Quadedge_handle;
    typedef typename Refs::Quadedge_const_handle Quadedge_const_handle;
    typedef typename Refs::Face_handle           Face_handle;
    typedef typename Refs::Face_const_handle     Face_const_handle;
    typedef typename Refs::Quadedge              Quadedge;
    typedef typename Refs::Face                  Face;
private:
    Point   p;
public:
    QuadedgeDS_vertex_base() {}
    QuadedgeDS_vertex_base( const Point& pp) : p(pp) {}
    Point&                point()                           { return p; }
    const Point&          point() const                     { return p; }
};

template < class Refs, class P>
class QuadedgeDS_vertex_base< Refs, Tag_true, P> {
public:
    typedef Refs                                 QuadedgeDS;
    typedef QuadedgeDS_vertex_base< Refs, Tag_true, P>      Base;
    typedef Tag_true                             Supports_vertex_quadedge;
    typedef Tag_true                             Supports_vertex_point;
    typedef P                                    Point;
    typedef typename Refs::Vertex_handle         Vertex_handle;
    typedef typename Refs::Vertex_const_handle   Vertex_const_handle;
    typedef typename Refs::Quadedge_handle       Quadedge_handle;
    typedef typename Refs::Quadedge_const_handle Quadedge_const_handle;
    typedef typename Refs::Face_handle           Face_handle;
    typedef typename Refs::Face_const_handle     Face_const_handle;
    typedef typename Refs::Quadedge              Quadedge;
    typedef typename Refs::Face                  Face;
private:
    Quadedge_handle qdg;
    Point           p;
public:
    QuadedgeDS_vertex_base() {}
    QuadedgeDS_vertex_base( const Point& pp) : p(pp) {}
    Quadedge_handle       quadedge()                        { return qdg; }
    Quadedge_const_handle quadedge() const                  { return qdg; }
    void                  set_quadedge( Quadedge_handle q)  { qdg = q; }
    Point&                point()                           { return p; }
    const Point&          point() const                     { return p; }
};

} //namespace CGAL

#endif // CGAL_QUADEDGEDS_VERTEX_BASE_H //
// EOF //
