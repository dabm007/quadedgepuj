#ifndef CGAL_QUADEDGEDS_CUT_COMPONENT_H
#define CGAL_QUADEDGEDS_CUT_COMPONENT_H 1

#include <CGAL/basic.h>
#include <QuadedgeDS_decorator.h>

namespace CGAL_UPJ {


template < class QDS, class Predicate >
typename QDS::Quadedge_handle 
quadedgeDS_cut_component( QDS&                           qds,
                          typename QDS::Quadedge_handle  h,
                          Predicate                      pred,
                          typename QDS::Quadedge_handle& cut_piece)
// Cuts out a piece of a quadedge data structure for which the predicate
// `pred' is true for the vertices.
// The edge-vertex graph of the piece has to be a connected component.
// The remaining piece gets a new boundary. Returns a border quadedge of 
// the new boundary on the remaining piece. Assigns a quadedge of the 
// cut outpiece to `cut_piece'.
// The geometry for the vertices
// on the boundary and the hole have to be taken care of after this
// function call. The cut-out piece can be deleted with the member
// function erase_connected_component of the decorator class. It can
// technically happen that only an isolated vertex would remain in the
// cut out piece, in which case a dummy quadedge pair and vertex will be
// created to keep this vertex representable in the quadedge data structure.
// Precondition: pred( h->vertex()) && ! pred( h->opposite()->vertex()).
{
    typedef typename QDS::Vertex_handle    Vertex_handle;
    typedef typename QDS::Quadedge_handle  Quadedge_handle;
    typedef typename QDS::Face_handle      Face_handle;
    typedef typename QDS::Vertex           Vertex;
    typedef typename QDS::Quadedge         Quadedge;
    typedef typename QDS::Face             Face;
    CGAL::QuadedgeDS_decorator<QDS> D(qds);

    CGAL_precondition( D.is_valid(false,3));
    CGAL_precondition( pred( h->vertex()));
    CGAL_precondition( ! pred( h->opposite()->vertex()));

    Quadedge_handle start = h;
    Quadedge_handle hnew;
    Quadedge_handle hlast;
    while (true) {
        // search re-entry point
        Quadedge_handle g = h;
        while ( pred( g->next()->vertex())) {
            g = g->next();
            // create border edges around cap
            D.set_face( g, Face_handle());
        }
        if ( hnew == Quadedge_handle()) {
            // first edge, special case
            CGAL_assertion( g->next() != h && g->next()->opposite() != h);
            Quadedge_handle gnext = g->next()->opposite();
            D.remove_tip( g);
            Vertex_handle v = D.vertices_push_back( Vertex());
            D.close_tip( gnext, v);
            hnew = qds.edges_push_back( Quadedge(), Quadedge());
            hlast = hnew->opposite();
            D.insert_tip( hlast, gnext);
            D.set_face( hnew, D.get_face( gnext));
            D.set_face_quadedge( hnew);
            h = g;
            D.set_vertex_quadedge( h);                
        } else { // general case and last case
            Quadedge_handle gnext = g->next()->opposite();
            if ( gnext == start && gnext == g) {
                // last edge, special case of isolated vertex.
                // Create dummy edge and dummy vertex and attach it to g
                g = qds.edges_push_back( Quadedge(), Quadedge());
                D.insert_tip( g, gnext);
                D.close_tip(g->opposite(), D.vertices_push_back(Vertex()));
                D.set_vertex_quadedge( g);                
                D.set_vertex_quadedge( g->opposite());                
            }
            D.remove_tip( g);
            Vertex_handle v = D.vertices_push_back( Vertex());
            D.close_tip( hnew, v);
            D.insert_tip( gnext, hnew);
            hnew = qds.edges_push_back( Quadedge(), Quadedge());
            D.insert_tip( hnew->opposite(), gnext);
            D.set_face( hnew, D.get_face( gnext));
            D.set_face_quadedge( hnew);
            h = g;
            D.set_vertex_quadedge( h);                
            if ( gnext == start) {
                // last edge, special
                D.insert_tip( hnew, hlast);
                break;
            }
        }
    } // while(true)
    Face_handle fnew = D.faces_push_back( Face());
    D.set_face_in_face_loop( hlast, fnew);
    D.set_face_quadedge( hlast);
    cut_piece = h;
    CGAL_postcondition( D.is_valid(false,3));
    return hlast;
}

template < class QDS, class Predicate >
typename QDS::Quadedge_handle 
quadedgeDS_cut_component( QDS&                           qds,
                          typename QDS::Quadedge_handle  h,
                          Predicate                      pred)
// Same function as above, but deletes the cut out piece immediately.
{
    typedef typename QDS::Quadedge_handle  Quadedge_handle;
    CGAL::QuadedgeDS_decorator<QDS> D(qds);

    CGAL_precondition( D.is_valid(false,3));
    CGAL_precondition( pred( h->vertex()));
    CGAL_precondition( ! pred( h->opposite()->vertex()));
    Quadedge_handle cut_piece;
    Quadedge_handle hnew = quadedgeDS_cut_component( qds, h, pred, cut_piece);
    D.erase_connected_component( cut_piece);
    CGAL_postcondition( D.is_valid(false,3));
    return hnew;
}   

} //namespace CGAL
#endif // CGAL_QUADEDGEDS_CUT_COMPONENT_H //
// EOF //
