#ifndef CGAL_QUADEDGEDS_CONNECTED_COMPONENTS_H
#define CGAL_QUADEDGEDS_CONNECTED_COMPONENTS_H 1

#include <CGAL/Unique_hash_map.h>
#include <CGAL/iterator.h>
#include <vector>

namespace CGAL_UPJ {

template <class QDS, class Output_iterator, 
          class Quadedge_iterator, class Quadedge_handle>
std::size_t 
quadedgeds_connected_components( QDS qds, Output_iterator result) {
    Unique_hash_map< Quadedge_iterator, bool> qedge_map( false);
    std::size_t count = 0;
    std::vector< Quadedge_handle> hstack;
    hstack.reserve( qds.size_of_quadedges() + 3/ 4);
    Quadedge_iterator scan = qds.quadedges_begin();
    while ( scan != qds.quadedges_end()) {
        // first trace the component, then report it
        hstack.clear();
        hstack.push_back( scan);
        while ( ! hstack.empty()) {
            Quadedge_handle q = hstack.back();
            hstack.pop_back();
            if ( ! qedge_map[q] ) {
                qedge_map[q] = true;
                hstack.push_back( q->next());
                hstack.push_back( q->opposite());
            }
        }
        ++count;
        *result++ = scan;
        while( qedge_map[ scan])
            ++scan;
    }
    return count;
}

template <class QDS, class Output_iterator>
std::size_t 
quadedgeds_connected_components( QDS& qds, Output_iterator result) {
    typedef typename QDS::Quadedge_iterator Quadedge_iterator; 
    typedef typename HDS::Quadedge_handle   Quadedge_handle; 
    return quadedgeds_connected_components<QDS&, Output_iterator, 
        Quadedge_iterator, Quadedge_handle>( qds, result);
}

template <class QDS, class Output_iterator>
std::size_t 
quadedgeds_connected_components( const QDS& qds, Output_iterator result) {
    typedef typename QDS::Quadedge_const_iterator Quadedge_iterator; 
    typedef typename QDS::Quadedge_const_handle   Quadedge_handle; 
    return quadedgeds_connected_components<const QDS&, Output_iterator, 
        Quadedge_iterator, Quadedge_handle>( qds, result);
}

template <class QDS>
std::size_t 
quadedgeds_connected_components( QDS& qds) {
    return quadedgeds_connected_components( qds, Emptyset_iterator());
}


} //namespace CGAL
#endif // CGAL_QUADEDGEDS_CONNECTED_COMPONENTS_H //
// EOF //
