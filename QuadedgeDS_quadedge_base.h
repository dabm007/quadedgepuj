#ifndef CGAL_QUADEDGEDS_QUADEDGE_BASE_H
#define CGAL_QUADEDGEDS_QUADEDGE_BASE_H 1

#include <CGAL/basic.h>

typedef CGAL::Tag_true	Tag_true;
typedef CGAL::Tag_false	Tag_false;

namespace CGAL_UPJ {

template < class _TRefs, class _TPrimality,class TP, class TV, class TF>
struct QuadedgeDS_quadedge_base_base {
    typedef _TRefs                               QuadedgeDS;
    typedef QuadedgeDS_quadedge_base_base<_TRefs,_TPrimality> Base_base;
    typedef _TPrimality                            Supports_quadedge_orot;
    typedef TP                                   Supports_halfedge_prev;
    typedef TV                                   Supports_halfedge_vertex;
    typedef TF                                   Supports_halfedge_face;

    typedef typename _TRefs::Vertex_handle         Vertex_handle;
    typedef typename _TRefs::Vertex_const_handle   Vertex_const_handle;
    typedef typename _TRefs::Quadedge_handle       Quadedge_handle;
    typedef typename _TRefs::Quadedge_const_handle Quadedge_const_handle;
    typedef typename _TRefs::Face_handle           Face_handle;
    typedef typename _TRefs::Face_const_handle     Face_const_handle;
    typedef typename _TRefs::Vertex                Vertex;
    typedef typename _TRefs::Face                  Face;
private:
    Quadedge_handle  onext;
    Quadedge_handle  orot;
public:
    Quadedge_handle       get_orot()                        { return orot;}
    Quadedge_const_handle get_orot() const                  { return orot;}
    void                  set_orot( Quadedge_handle h)  { orot = h;}
    Quadedge_handle       get_onext()                            { return onext;}
    Quadedge_const_handle get_onext() const                      { return onext;}
    void                  set_onext( Quadedge_handle h)      { onext = h;}

//????
    bool is_border() const { return false;}
//????
};
// en el constructor: std::cout << "_Z" << typeid( ).name( _TRefs ) << std::endl;
template < class _TRefs, class _TPrimality = Tag_true >
class QuadedgeDS_quadedge_base;

template < class _TRefs >
class QuadedgeDS_quadedge_base< _TRefs, Tag_true> //PRIMALITY in true => Primal
    : public QuadedgeDS_quadedge_base_base<_TRefs, Tag_true>
{
public:
    typedef typename _TRefs::Quadedge_handle       Quadedge_handle;
    typedef QuadedgeDS_quadedge_base<_TRefs, Tag_true>
        Base;
    typedef QuadedgeDS_quadedge_base_base<_TRefs, Tag_true>
        Base_base;
private:
    void  set_orot( Quadedge_handle h)  { Base_base::set_orot(h);}
    void  set_onext( Quadedge_handle h)  { Base_base::set_onext(h);}
    Quadedge_handle get_onext()		{ Base_base::get_onext();}
    Quadedge_handle get_orot()		{ Base_base::get_orot();}

};

template < class _TRefs >
class QuadedgeDS_quadedge_base< _TRefs, Tag_false> //PRIMALITY in false => Dual
    : public QuadedgeDS_quadedge_base_base<_TRefs, Tag_false>
{
public:
    typedef typename _TRefs::Quadedge_handle       Quadedge_handle;
    typedef QuadedgeDS_quadedge_base<_TRefs, Tag_false>
        Base;
    typedef QuadedgeDS_quadedge_base_base<_TRefs, Tag_false>
        Base_base;
private:
    void  set_orot( Quadedge_handle h)  { Base_base::set_orot(h);}
    void  set_onext( Quadedge_handle h)  { Base_base::set_onext(h);}
    Quadedge_handle get_onext()		{ Base_base::get_onext();}
    Quadedge_handle get_orot()		{ Base_base::get_orot();}

};

} //namespace CGAL_UPJ

#endif // CGAL_QUADEDGEDS_QUADEDGE_BASE_H //
// EOF //
