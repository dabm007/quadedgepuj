#ifndef CGAL_QUADEDGEDS_DEFAULT_H
#define CGAL_QUADEDGEDS_DEFAULT_H 1

#include "QuadedgeDS_items_2.h"
#include "QuadedgeDS_list.h"
#include <CGAL/memory.h>

namespace CGAL_UPJ {

template <class Traits_, class QuadedgeDSItems = QuadedgeDS_items_2, 
          class Alloc = CGAL_ALLOCATOR(int)>
class QuadedgeDS_default 
    : public QuadedgeDS_list< Traits_, QuadedgeDSItems, Alloc> {
public:
    typedef Traits_                                          Traits;
    typedef QuadedgeDS_list<Traits_, QuadedgeDSItems, Alloc> D_S;
    typedef typename D_S::size_type                           size_type;
    QuadedgeDS_default() {}
    QuadedgeDS_default( size_type v, size_type h, size_type f)
        : QuadedgeDS_list< Traits_, QuadedgeDSItems, Alloc>(v,h,f) {}
};
#define CGAL_QUADEDGEDS_DEFAULT_H  ::CGAL::QuadedgeDS_default

} //namespace CGAL
#endif // CGAL_HALFEDGEDS_DEFAULT_H //
// EOF //
