#ifndef CGAL_QUADEDGEDS_ITEMS_2_H
#define CGAL_QUADEDGEDS_ITEMS_2_H 1
#include <QuadedgeDS_vertex_base.h>
#include <QuadedgeDS_quadedge_base.h>
#include <QuadedgeDS_face_base.h>

namespace CGAL_UPJ {

class QuadedgeDS_items_2 {
public:
    template < class Refs, class Traits>
    struct Vertex_wrapper {
        typedef typename Traits::Point_3 Point;
        typedef QuadedgeDS_vertex_base< Refs, Tag_true, Point> Vertex;
    };
    template < class Refs, class Traits>
    struct Quadedge_wrapper {
        typedef QuadedgeDS_quadedge_base<Refs>                Quadedge;
    };
    template < class Refs, class Traits>
    struct Face_wrapper {
        typedef QuadedgeDS_face_base< Refs>                    Face;
    };
};

} //namespace CGAL
#endif // CGAL_QUADEDGEDS_ITEMS_2_H //
// EOF //
