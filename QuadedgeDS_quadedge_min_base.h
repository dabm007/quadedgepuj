#ifndef CGAL_QUADEDGEDS_QUADEDGE_MIN_BASE_H
#define CGAL_QUADEDGEDS_QUADEDGE_MIN_BASE_H 1

#include <CGAL/basic.h>

namespace CGAL_UPJ {

template < class Refs>
struct QuadedgeDS_quadedge_min_base_base {
    // Base_base will be used to access set_opposite(), which is
    // made private in the normal halfedge bases. Since quadedges
    // come always in pairs, managed by the QDS, the set_opposite()
    // member function is protected from the user.
    typedef Refs                                 QuadedgeDS;
    typedef QuadedgeDS_quadedge_min_base_base<Refs> Base_base;
    typedef Tag_false                            Supports_quadedge_prev;
    typedef Tag_false                            Supports_quadedge_vertex;
    typedef Tag_false                            Supports_quadedge_face;
    typedef typename Refs::Vertex_handle         Vertex_handle;
    typedef typename Refs::Vertex_const_handle   Vertex_const_handle;
    typedef typename Refs::Quadedge_handle       Quadedge_handle;
    typedef typename Refs::Quadedge_const_handle Quadedge_const_handle;
    typedef typename Refs::Face_handle           Face_handle;
    typedef typename Refs::Face_const_handle     Face_const_handle;
    typedef typename Refs::Vertex                Vertex;
    typedef typename Refs::Face                  Face;
private:
    Quadedge_handle  opp;
    Quadedge_handle  nxt;
public:
    Quadedge_handle       opposite()                        { return opp;}
    Quadedge_const_handle opposite() const                  { return opp;}
    void                  set_opposite( Quadedge_handle h)  { opp = h;}
    Quadedge_handle       next()                            { return nxt;}
    Quadedge_const_handle next() const                      { return nxt;}
    void                  set_next( Quadedge_handle h)      { nxt = h;}

    bool is_border() const { return false;}
        // is always false as long as faces are not supported.
};

template < class Refs>
class QuadedgeDS_quadedge_min_base
    : public QuadedgeDS_quadedge_min_base_base< Refs>
{
public:
    typedef typename Refs::Quadedge_handle Quadedge_handle;
    typedef QuadedgeDS_quadedge_min_base<Refs>       Base;
    typedef QuadedgeDS_quadedge_min_base_base<Refs>  Base_base;
private:
    void  set_opposite( Quadedge_handle h)  { Base_base::set_opposite(h);}
};

} //namespace CGAL

#endif // CGAL_QUADEDGEDS_QUADEDGE_MIN_BASE_H //
// EOF //
